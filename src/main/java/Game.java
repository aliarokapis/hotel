package hotel;

import hotel.Player;
import hotel.Hotel;
import hotel.Board;
import hotel.GameState;
import hotel.phases.*;
import hotel.util.function.*;
import hotel.util.creators.TileCreator;
import hotel.ui.UI;
import hotel.ui.TerminalUI;
import java.util.function.*;
import java.util.Random;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.concurrent.CompletableFuture;

/**
 * Class implementing the main game loop.
 * This class uses the different phase classes, provides
 * them with their required dependencies and puts them together in order to 
 * compose the main state machine. 
 */

public class Game implements Runnable
{
	/** 
	 * Constructor.
         * @param state The GameState object required to supply the data-specific dependencies to the different phase objects.
	 * @param ui The UI object required in order to provide the visualization dependencies to the different phase objects.
	 */

	public Game(GameState state, UI ui)
	{
		this.state = state;
		this.ui = ui;
		
		this.startOfTurnPhase = 
			() -> new StartOfTurnPhase(
                                (p) -> this.ui.newTurn(p),
				() -> this.state.getNumberOfPlayers(),
				() -> this.state.getNextPlayer(),
				(p) -> { return this.endOfGamePhase.apply(p); },
				(p) -> { return this.playerRollsDicePhase.apply(p); });
				
			
		Random random = new Random();
	
		Supplier<Integer> dice = () -> this.ui.rollDice(random);

		this.playerRollsDicePhase = 
			(player) -> new PlayerRollsDicePhase(
				player,		
				dice,
				() -> player.getLocation(),
				(l, n) -> this.state.nextLocation(l, n),
				(l) -> this.state.hasPlayer(l),
				this.playerIsMovingPhase
			);


		this.playerIsMovingPhase = 
			(player, rem) -> new PlayerIsMovingPhase(
				player,
				rem,
				() -> player.getLocation(),
				(l, n) -> this.state.nextLocation(l, n),
				(l) -> { 
					this.ui.movePlayer(player, l); 
					player.setLocation(l);
				},
				() -> this.state.getTile(player.getLocation()),
				(t, i) -> { return this.playerPassesThroughPhase.apply(player, t, i); }
			);
				
		this.playerPassesThroughPhase = 
			(player, tile, rem) -> new PlayerPassesThroughPhase(
				player,
				tile,
				rem,
				this.playerPassesThroughBankTilePhase,
				this.playerPassesThroughTownHallTilePhase,
				this.playerDonePassingThroughPhase
			);

		this.playerPassesThroughBankTilePhase = 
			(player, tile, rem) -> new PlayerPassesThroughBankPhase(
				player,
				n -> this.ui.playerIsPaidByBank(player, n),
				() -> this.playerDonePassingThroughPhase.apply(player, tile, rem)
			);

		this.playerPassesThroughTownHallTilePhase = 
			(player, tile, rem) -> new PlayerPassesThroughTownHallPhase(
				player,
				() -> this.state.getHotels().stream(),
				h -> this.state.neighboringLocations(h),
				l -> this.state.getHotelEntrance(l).isPresent(),
				hots -> this.ui.showAvailableHotelsForEntrance(player, hots),
				() -> this.ui.chooseToPutEntrance(player),
		                h -> this.playerChoseToPutHotelEntrancePhaseHelper.apply(
					player, 
					h, 
					this.playerDonePassingThroughPhase.apply(player, tile, rem)),
				() -> this.playerDonePassingThroughPhase.apply(player, tile, rem)
			);

		this.playerDonePassingThroughPhase = 
			(player, tile, rem) -> new PlayerDonePassingThroughPhase(
				player,
				tile,
				rem,
				this.playerIsMovingPhase,
				this.playerHandleEntrancePhase
			);

		this.playerMovedToPhase =
			(player, tile) -> new PlayerMovedToPhase(
				player,
				tile,
				() -> { return this.playerOnPurchaseTilePhase.apply(player); },
				() -> { return this.playerOnBuildTilePhase.apply(player); },
				this.startOfTurnPhase
			);
				
		this.playerHandleEntrancePhase = 
			(player, tile) -> new PlayerHandleEntrancePhase(
				player,
				(l) -> this.state.getHotelEntrance(l),
				dice,
				(o, pr) -> this.ui.playerPayment(player, o, pr),
				() -> { 
					this.ui.bankruptcy(player);
					this.state.removePlayer(player);
				},
				this.startOfTurnPhase,
				() -> this.playerMovedToPhase.apply(player, tile)
			);


		this.playerOnPurchaseTilePhase = 
			(player) -> new PlayerOnPurchaseTilePhase(
				player,
				l -> this.state.neighboringHotels(l),
				(hots) -> this.ui.showAvailableHotelsForPurchase(player, hots),
				() -> this.ui.chooseToPurchase(player),
				h -> this.playerChoseToPuchaseHotelPhase.apply(player, h),
				this.startOfTurnPhase
			);
				

		this.playerChoseToPuchaseHotelPhase = 
			(player, hots) -> new PlayerChoseToPurchaseHotelPhase(
				player,	
				hots,
				() -> this.ui.chooseHotel(player, hots),
				h -> this.ui.playerPurchased(player, h),
				(o, h) -> this.ui.playerRepurchasedFrom(player, o, h),
				() -> this.playerOnPurchaseTilePhase.apply(player),
				this.startOfTurnPhase
				);

		this.playerOnBuildTilePhase = 
			(player) -> new PlayerOnBuildTilePhase(
				player,
				() -> this.state.getHotels().stream(),
				(h) -> this.state.neighboringLocations(h),
				(l) -> this.state.getHotelEntrance(l).isPresent(),
				(hots) -> this.ui.showAvailableHotelsForEntrance(player, hots),
				(hots) -> this.ui.showAvailableHotelsForUpgrade(player, hots),
				(entr, upgr) -> this.ui.chooseUpgradeEntranceOrBoth(player, entr, upgr),
				h -> this.playerChoseToUpgradeHotelPhase.apply(player, h),
				h -> this.playerChoseToPutHotelEntrancePhase.apply(player, h),
                                this.startOfTurnPhase
				);
				
		this.playerChoseToUpgradeHotelPhase = 
			(player, hots) -> new PlayerChoseToUpgradeHotelPhase(
				player,
				hots,
				() -> this.ui.chooseHotel(player, hots),
				() -> { 
					
					Integer range = random.nextInt(1000);
					if (range < 500)
						return 0;
					if (range < 700)
						return 1;
					if (range < 850)
						return 2;
					return 3;
				},
				(h) -> this.ui.normalBuildingApproved(player, h),
				(h) -> this.ui.buildingNotApproved(player, h),
				(h) -> this.ui.freeBuildingApproved(player, h),
				(h) -> this.ui.overCostBuildingApproved(player, h),
				() -> this.playerOnBuildTilePhase.apply(player),
				this.startOfTurnPhase
				);
				
		this.playerChoseToPutHotelEntrancePhase = 
			(player, hots) -> this.playerChoseToPutHotelEntrancePhaseHelper.apply(
						player, 
						hots,
						this.startOfTurnPhase.get());

		this.playerChoseToPutHotelEntrancePhaseHelper = 
			(player, hots, next) -> new PlayerChoseToPutHotelEntrancePhase(
				player,
				hots,
				() -> this.ui.chooseHotel(player, hots),
				(h) -> this.state.neighboringLocations(h),
				(l) -> this.state.getHotelEntrance(l).isPresent(),
				(l,h) -> {
                                        this.ui.setEntrance(player, l, h);
                                        this.state.addHotelEntrance(l, h);
                                },
				() -> this.playerOnBuildTilePhase.apply(player),
				() -> next
				);



		this.endOfGamePhase = 
			(player) -> { 
					this.ui.gameOver(player);
					return new EndOfGamePhase(player); 
				};

	}

	/** 
	 * Runs the game loop. Phases are retrieved in succession until there are no phases left.
         */
	public void run() 
	{
	    Phase phase = this.startOfTurnPhase.get();

	    while (phase != null)
	    {
		phase = phase.next();
	    }
	}

	private GameState state;
	private UI ui;

	private Supplier<Phase> startOfTurnPhase;

	private Function1<Player, Phase> playerRollsDicePhase;
	private Function2<Player, Integer, Phase> playerIsMovingPhase;
	private Function3<Player, Tile, Integer, Phase> playerPassesThroughPhase;
	private Function3<Player, Tile, Integer, Phase> playerPassesThroughBankTilePhase;
	private Function3<Player, Tile, Integer, Phase> playerPassesThroughTownHallTilePhase;
	private Function3<Player, Tile, Integer, Phase> playerDonePassingThroughPhase;
	private Function2<Player, Tile, Phase> playerMovedToPhase;
	private Function1<Player, Phase> playerOnBuildTilePhase;
	private Function2<Player, List<Hotel>, Phase> playerChoseToUpgradeHotelPhase;
	private Function2<Player, List<Hotel>, Phase> playerChoseToPutHotelEntrancePhase;
	private Function3<Player, List<Hotel>, Phase, Phase> playerChoseToPutHotelEntrancePhaseHelper;
	private Function1<Player, Phase> playerOnPurchaseTilePhase;
	private Function2<Player, List<Hotel>, Phase> playerChoseToPuchaseHotelPhase;
	private Function2<Player, Tile, Phase> playerHandleEntrancePhase;
	private Function1<Player, Phase> endOfGamePhase;

}
