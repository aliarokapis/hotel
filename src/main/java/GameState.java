package hotel;

import hotel.Player;
import hotel.Tile;
import hotel.Hotel;
import hotel.Board;
import java.util.stream.Stream;
import java.util.Collections;
import java.util.Optional;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

/** 
 * Class representing the state of the Game.
 * It provides info regarding the players, hotels, tiles, path and entrances
 * and provides the functionality required in order to supply the data-specific dependencies
 * to the phase objects.
 *
 * The class does not prevent external modification of the players, hotels, tiles and path given
 * since it simply provides convenient functionality. It does however encapsulate the entrances.
 */
public class GameState
{
	/**
	 * Constructor.
	 * @param players The players participating in the game.
	 * @param tiles The list containing the tiles of the main path.
	 * @param hotels The available hotels of the map.
	 * @param path The Board.Coordinates representing the main path that the players follow.
	 * @param board The Board representing the map.
	 */
	public GameState(List<Player> players, 
                         List<Tile> tiles, 
                         List<Hotel> hotels,
                         List<Board.Coordinates> path,
                         Board<String> board)
                         
	{
                this.players = players;
		this.tiles = tiles;
		this.hotels = hotels;
		this.path = path;
                this.board = board;
                this.entrances = new ArrayList<Hotel>(Collections.nCopies(tiles.size(), null));
	}

	/**
	 * Gets the players participating in the game.
	 * @return The Players participatin in the game.
	 */
	public List<Player> getPlayers()
	{
		return players;
	}

	/**
	 * Gets the Tiles of the main path that the players follow.
	 * @return The Tiles of the main path.
	 */
        public List<Tile> getTiles()
	{
		return tiles;
	}

	/**
	 * Gets the main path that the players follow.
	 * @return The main path.
	 */
        public List<Board.Coordinates> getPath()
	{
		return path;
	}

	/**
	 * Gets the Board that represents the map.
	 * @return The Board.
	 */
        public Board<String> getBoard()
	{
		return board;
	}

	/**
	 * Gets the hotels available in the game.
	 * @return The Hotels available in the game.
	 */
        public List<Hotel> getHotels()
        {
                return hotels;
        }

	/**
	* Gets the location that corresponds the main path index after n moves from the specified location.
        * @param location The specified location.
	* @param n The moves to perform.
	* @return The resulting location in the main path.
	*/
	public Integer nextLocation(Integer location, Integer n)
	{
		return (location + n) % tiles.size();
	}

	/** 
	 * Checks whether the tile at the specified location contains a Player.
	 * @param location The specified location.
	 * @return true if the locationo contains  Player, false otherwise.
	 */
	public Boolean hasPlayer(Integer location)
	{
		for (Player player : this.players)
		{
			if (player.getLocation() == location)
				return true;
		}

		return false;
	}

	/**
	 * Removes the specified player from the game. This function
	 * takes care to also remove any associated entrances and 
	 * give the hotels of the Player to the bank.
	 * @param player The player to remove.
	 */
	public void removePlayer(Player player)
	{
		players.remove(player);

		for (int i = 0; i != entrances.size(); ++i)
		{
			Optional<Hotel> entrance = getHotelEntrance(i);

			if (!entrance.isPresent()) 
				continue;

			Hotel hotelOfEntrance = entrance.get();

			if(hotelOfEntrance.getOwner().filter(p -> p == player).isPresent())
				removeHotelEntrance(i);
		}

		for (Hotel hotel : hotels)
		{
			if (hotel.getOwner().filter(p -> p == player).isPresent())
				hotel.setBoughtByBank(true);
		}
	}

	/**
	 * Gets the next player in order.
         * Successive calls to the function return the Players in the order provided in the 
	 * constructor.
	 * @return The next player in order.
	 */
	 
	public Player getNextPlayer()
	{
		Player next = players.get(0);
		Collections.rotate(players, -1);
		return next;
	}

	/**
	 * The number of players participating in the game.
	 * @return The number of players.
	 */
	public Integer getNumberOfPlayers()
	{
		return players.size();
	}

	/**
	 * Gets the tile at the specified location in the path.
	 * @param location The specified location.
	 * @return The associated tile.
	 */
	public Tile getTile(Integer location)
	{
		return tiles.get(location);
	}

	/**
	 * Returns the hotel that corresponds to the entrance at the specified location if the entrance exists.
	 * @param location The specified location.
	 * @return If there is an entrance at the specified location, returns a non-empty Optional of it's associated Hotel, returns an empty Optional otherwise.
	 */
        public Optional<Hotel> getHotelEntrance(Integer location)
        {
                return Optional.ofNullable(entrances.get(location));
        }

	/**
	 * Adds a hotel entrance at the specified location in the main path.
	 * @param location The specified location to the put the entrance.
	 * @param hotel The associated hotel of the entrance.
	 */
        public void addHotelEntrance(Integer location, Hotel hotel)
        {
                entrances.set(location, hotel);
        }

	/**
	 * Removes any hotel entrance from the specified location in the main path.
	 * @param location The specified location to remove any entrance.
	 */
	public void removeHotelEntrance(Integer location)
	{
		entrances.set(location, null);
	}

	/**
	 * Returns the Hotel with the specified id if such a Hotel exists.
	 * @param id The Hotel id to search for.
	 * @return If there is a hotel with such id returns a non-empty Optional, otherwise returns an empty Optional.
	 */
	public Optional<Hotel> hotelWithId(Integer id)
	{
		for (Hotel hotel : hotels)
		{		
			if (hotel.getId().equals(id))	
				return Optional.of(hotel);
		}

		return Optional.empty();
	}

	/**
	 * Retrieves a Stream of any hotels that are neighbors of the tile at the specified location in the main path.
	 * @param location The specified location.
	 * @return A Stream of the neighboring hotels.
	 */
        public Stream<Hotel> neighboringHotels(Integer location)
        {
                Board.Coordinates coords = path.get(location);
                Stream<Board.Coordinates> neighbors = board.getNeighborsOfCoordinates(coords.x, coords.y);

		Stream<Optional<Hotel>> hotels = neighbors.map((nCoords) ->
		{
			try 
			{
				String piece = board.getPieceAt(nCoords.x, nCoords.y);
				Integer id = Integer.parseInt(piece);
				Optional<Hotel> mhotel = hotelWithId(id);
				return mhotel;
			} 
			catch (NumberFormatException e) {
                                return Optional.empty();
                        }
		});

		return hotels.flatMap(Optional::stream);
        }

	/**
	 * Retrieves a Stream of locations of tiles in the main path that neighbor the specified Hotel.
	 * @param hotel The specified Hotel.
	 * @return A Stream of neighboring locations in the main path.
	 */
        public Stream<Integer> neighboringLocations(Hotel hotel)
        {
                Set<Board.Coordinates> neighbors = board.getNeighborsOfPiece(Integer.toString(hotel.getId()));
                return neighbors.stream()
				.filter((c) -> Set.of("H", "S", "E").contains(board.getPieceAt(c.x, c.y)))
				.map((c) -> path.indexOf(c))
				.filter((c) -> c >= 0);
        }

	/**
	 * Retrieves a Stream of the Hotels of the specified Player.
	 * @param player The player whose hotels to retrieve.
	 * @return A Stream of the player's hotels.
	 */
        public Stream<Hotel> getHotelsOf(Player player)
        {
                return hotels.stream().filter((h) -> h.getOwner().filter(p -> p == player).isPresent());
        }

	private List<Player> players;
	private List<Tile> tiles;
	private List<Hotel> hotels;
	private List<Hotel> entrances;
        private List<Board.Coordinates> path;
        private Board<String> board;
}
