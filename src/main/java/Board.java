package hotel;

import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.HashSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.stream.Stream;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Class used to perform convenient queries on 2d boards.
 */
public class Board<T>
{
	/** 
	 * Class representing coordinates on a 2d board.
	 */
	public static class Coordinates
	{
		/**
		 * Constructor.
		 * @param x The x coordinate.
		 * @param y The y coordinate.
		 */
		public Coordinates(int x, int y)
		{
			this.x = x;
			this.y = y;
		}


		/**
		 * Equality function to support non-reference comparison.
		 */
                @Override
                public boolean equals(Object me) {
                    Coordinates cme = (Coordinates)me;
                    if(x==cme.x && y==cme.y)
                        return true;
                    else 
                        return false;
                }   

		/**
		 * Hash function to support hashing.
		 */
                @Override
                public int hashCode() {
                    int res = this.x;
                    res ^= this.y + 0x9e3779b9 + (this.x << 6) + (this.y >> 2);
                    return res;
                }

		public int x;
		public int y;
	}

	/** 
	 * Constructor.
	 * @param raw A List of Lists of equal size representing a 2d board.
	 */
	public Board(List<List<T>> raw)
	{
		this.rows = raw.size();
		this.columns = raw.stream().findAny().map((c) -> c.size()).orElse(0);
		this.coordinates = new HashMap<T, List<Coordinates>>();
		this.neighbors = new HashMap<T, Set<Coordinates>>();
		this.ids = new ArrayList<T>();

		ListIterator<List<T>> rowIterator = raw.listIterator();

		while (rowIterator.hasNext())
		{
			int y = rowIterator.nextIndex();
			List<T> row = rowIterator.next();
			ListIterator<T> colIterator = row.listIterator();

			while (colIterator.hasNext())
			{
				int x = colIterator.nextIndex();
				T id = colIterator.next();

				ids.add(id);

				List lst = coordinates.get(id);

				Coordinates coords = new Coordinates(x, y);

				if (lst == null)
				{
					List nlst = new ArrayList<Coordinates>();
					nlst.add(coords);
					coordinates.put(id, nlst);
				}
				else
				{
					lst.add(coords);
				}
			}
		}

		if (ids.size() != this.rows * this.columns)
			throw new IllegalArgumentException("The provided data should form a 2d array");

	}


	/**
	 * Retrieves the number of rows of the board.
	 * @return The number of rows.
	 */
	public int rows()
	{
		return rows;
	}

	/**
	 * Retrieves the number of columns of the board.
	 * @return The number of columns.
	 */
	public int columns()
	{
		return columns;
	}


	/**
	 * Checks whether the given coordinates are valid.
	 * @param x The x coordinate.
	 * @param y The y coordinate.
	 * @return true if (x,y) is contained in the board, false otherwise.
	 */
	public boolean areValidCoordinates(int x, int y)
	{
		return (y >= 0 && y < rows && x >= 0 && x < columns);
	}

	/**
	 * Retrieves the Coordinates of the occurrencies of the given piece in the board.
	 * @param id The piece, the coordinates of which should be returned.
	 * @return The Coordinate s of the occurrencies of the given piece.
	 */
	public List<Coordinates> getCoordinatesOfPiece(T id)
	{
		List<Coordinates> lst = coordinates.get(id);

		if (lst == null)
			return List.of();

		return Collections.unmodifiableList(lst);
	}

	/**
	 * Gets piece at the specified Coordinates.
	 * @param x The x coordinate.
	 * @param y The y coordinate.
	 * @return The piece at the specified Coordinates.
	 */
	public T getPieceAt(int x, int y)
	{
		if (!areValidCoordinates(x, y))
			throw new IllegalArgumentException("Coordinates should be valid");

		return ids.get(y * columns + x);
	}

	private Stream<Coordinates> getNeighborsOfCoordinatesUnchecked(int x, int y)
	{
		List<Coordinates> neighbs = new ArrayList<Coordinates>();

		neighbs.add(new Coordinates(x-1, y));
		neighbs.add(new Coordinates(x+1, y));
		neighbs.add(new Coordinates(x, y-1));
		neighbs.add(new Coordinates(x, y+1));

		return neighbs.stream();
	}

	/**
	 * Gets the neighboring Coordinates of the given Coordinates.
	 * @param x The x coordinate.
	 * @param y The y coordinate.
	 * @return A stream of the neighboring valid Coordinates.
	 */
	public Stream<Coordinates> getNeighborsOfCoordinates(int x, int y)
	{
		return getNeighborsOfCoordinatesUnchecked(x, y)
					.filter((c) -> areValidCoordinates(c.x, c.y));
	}

	/**
	 * Retrieves the complete Set of neighbors of all the occurrences of the given Piece.
	 * @param id The given piece.
	 * @return The set of the neighboring Coordinates of the given piece.
	 */
	public Set<Coordinates> getNeighborsOfPiece(T id)
	{
		Set<Coordinates> neighbs = neighbors.get(id);

		if (neighbs == null)
		{
			neighbs = Collections.unmodifiableSet(
				getCoordinatesOfPiece(id)
					.stream() 
					.flatMap((c) -> getNeighborsOfCoordinatesUnchecked(c.x, c.y))
					.filter((c) -> areValidCoordinates(c.x, c.y))
					.filter((c) -> getPieceAt(c.x, c.y) != id)
					.collect(Collectors.toSet()));
			neighbors.put(id, neighbs);
		}

		return neighbs;
	}


	private List<T> ids; 
	private HashMap<T, List<Coordinates>> coordinates;
	private HashMap<T, Set<Coordinates>> neighbors;
	private int rows;
	private int columns;
}
