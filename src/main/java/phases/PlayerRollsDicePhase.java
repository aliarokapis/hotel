package hotel.phases;

import hotel.Player;
import hotel.phases.Phase;
import hotel.util.function.*;
import java.util.function.*;

/** Player rolls dice Phase.
 * Uses functional interfaces for dependency injection.
 * Calculates number of moves and moves to PlayerIsMovingPhase.
 */
public class PlayerRollsDicePhase implements Phase
{
	public PlayerRollsDicePhase(
		Player player,
		Supplier<Integer> dice,
		Supplier<Integer> getLocation,
		Function2<Integer, Integer, Integer> nextLocation,
		Predicate<Integer> hasPlayer,
		Function2<Player, Integer, Phase> playerIsMovingPhase)
	{
		this.player = player;
		this.dice = dice;
		this.getLocation = getLocation;
		this.nextLocation = nextLocation;	
		this.hasPlayer = hasPlayer;
		this.playerIsMovingPhase = playerIsMovingPhase;
	}

	public Phase next() 
	{ 
		Integer moves = dice.get();

                Integer currentLocation = getLocation.get();
		Integer next = nextLocation.apply(currentLocation, moves);
                
                while (hasPlayer.test(next))
                {
                       next = nextLocation.apply(next, 1);
                       moves += 1;
                }

                return playerIsMovingPhase.apply(player, moves);
	}

	private Player player;
	private Supplier<Integer> dice;
	private Supplier<Integer> getLocation;
	private Function2<Integer, Integer, Integer> nextLocation;
	private Predicate<Integer> hasPlayer;
	private Function2<Player, Integer, Phase> playerIsMovingPhase;		
}

