package hotel.phases;

import hotel.Player;
import hotel.Hotel;
import hotel.phases.Phase;
import hotel.util.function.*;
import java.util.function.*;
import java.util.Optional;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;

/**
 * Player has landed on a Purchase Tile Phase.
 * Uses functionl interfaces for dependency injection.
 * Checks if available hotel for purchases, if so asks the player, leads to PlayerChoseToPurchaseHotelPhase if so,
 * leads to next phase otherwise.
 */
public class PlayerOnPurchaseTilePhase implements Phase
{
	public PlayerOnPurchaseTilePhase(
		Player player,
		Function<Integer, Stream<Hotel>> neighboringHotels,
		Consumer1<List<Hotel>> notifyPlayerAvailableHotelsForPurchase,
		Supplier<Boolean> choseToPurchase,
		Function1<List<Hotel>, Phase> playerChoseToPurchaseHotelPhase,
		Supplier<Phase> next
	)
	{
		this.player = player;
		this.neighboringHotels = neighboringHotels;
		this.notifyPlayerAvailableHotelsForPurchase = notifyPlayerAvailableHotelsForPurchase;
		this.choseToPurchase = choseToPurchase;
		this.playerChoseToPurchaseHotelPhase = playerChoseToPurchaseHotelPhase;
		this.next = next;
	}

	public Phase next()
	{
		List<Hotel> hotelsAvailableForPurchase = 
			neighboringHotels.apply(player.getLocation())
				.filter(h -> !h.getOwner().filter(p -> p == player).isPresent())
				.filter(h -> h.getLevel() == 0)
				.collect(Collectors.toList());

		if (hotelsAvailableForPurchase.isEmpty())
			return next.get();

		notifyPlayerAvailableHotelsForPurchase.accept(hotelsAvailableForPurchase);

		if (choseToPurchase.get())
			return playerChoseToPurchaseHotelPhase.apply(
					hotelsAvailableForPurchase);

		return next.get();
	}

	private Player player;
	private Function<Integer, Stream<Hotel>> neighboringHotels;
	private Consumer1<List<Hotel>> notifyPlayerAvailableHotelsForPurchase;
	private Supplier<Boolean> choseToPurchase;
	private Function1<List<Hotel>, Phase> playerChoseToPurchaseHotelPhase;
	private Supplier<Phase> next;

}
