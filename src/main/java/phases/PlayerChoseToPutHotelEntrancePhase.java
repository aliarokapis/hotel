package hotel.phases;

import hotel.Player;
import hotel.Hotel;
import hotel.phases.Phase;
import hotel.util.function.*;
import java.util.function.*;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.List;
import java.util.Collections;
import java.util.Optional;
/**
 * Player Chose to Put Hotel Entrance Phase.
 * Uses functional interfaces for dependency injection.
 * Checks if player chose Hotel, if so performs put entrance logic, otherwise leads to Retry phase.
 * Leads to next phase afterwards.
 */

public class PlayerChoseToPutHotelEntrancePhase implements Phase
{
	public PlayerChoseToPutHotelEntrancePhase(
		Player player,
		List<Hotel> hotelsToPutEntranceChoices,
                Supplier<Optional<Hotel>> chooseHotel,
		Function1<Hotel, Stream<Integer>> neighboringLocations,
		Predicate<Integer> entranceAtLocation,
		Consumer2<Integer, Hotel> setEntranceAt,
		Supplier<Phase> retry,
		Supplier<Phase> next
	)
	{
		this.player = player;
		this.hotelsToPutEntranceChoices = hotelsToPutEntranceChoices;
		this.chooseHotel = chooseHotel;
                this.neighboringLocations = neighboringLocations;
		this.entranceAtLocation = entranceAtLocation;
                this.setEntranceAt = setEntranceAt;
                this.retry = retry;
		this.next = next;
	}

	public Phase next()
	{
		Optional<Hotel> mhotel = chooseHotel.get();

                if (!mhotel.isPresent())
                        return retry.get();

                Hotel hotel = mhotel.get();

		List<Integer> locations = 
			neighboringLocations.apply(hotel)
					    .filter(l -> !entranceAtLocation.test(l))
					    .collect(Collectors.toList());

		Collections.shuffle(locations);

		Integer location = locations.get(0);

		setEntranceAt.accept(location, hotel);
		player.setBalance(player.getBalance() - hotel.getBuildEntranceCost());
		
		return next.get();
	}

	private Player player;
	private List<Hotel> hotelsToPutEntranceChoices;
        private Supplier<Optional<Hotel>> chooseHotel;
	private Function1<Hotel, Stream<Integer>> neighboringLocations;
	private Predicate<Integer> entranceAtLocation;
	private Consumer2<Integer, Hotel> setEntranceAt;
	private Supplier<Phase> retry;
	private Supplier<Phase> next;
}
