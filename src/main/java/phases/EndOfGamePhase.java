package hotel.phases;

import hotel.phases.Phase;
import hotel.Player;

/**
 * End of the game Phase.
 * Returns null showcasing the end of the game loop.
 */
public class EndOfGamePhase implements Phase
{
	public EndOfGamePhase(Player winner)
	{
		this.winner = winner;
	}

	public Phase next() 
	{
		return null;
	}

	private Player winner;
}
