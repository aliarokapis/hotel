package hotel.phases;

import hotel.phases.Phase;
import hotel.util.function.*;
import java.util.function.*;
import hotel.Player;

/**
 * Phase called when the turn starts.
 * Uses Functional interfaces for dependenc injection.
 * Responsible for getting the next player.
 * Leads to the PlayerRollsDicePhase or EndOfGamePhase depending on the number of remaining players.
 */
public class StartOfTurnPhase implements Phase
{
	public StartOfTurnPhase(
		Consumer1<Player> notifyNewTurn,
		Supplier<Integer> getSize,
		Supplier<Player> getNext,
		Function1<Player, Phase> endOfGamePhase,
		Function1<Player, Phase> playerRollsDicePhase)
	{
		this.notifyNewTurn = notifyNewTurn;
		this.getSize = getSize;
		this.getNext = getNext;
		this.endOfGamePhase = endOfGamePhase;
		this.playerRollsDicePhase = playerRollsDicePhase;
	}

	public Phase next() 
	{
                Phase next = null;

                Player player = getNext.get();

		notifyNewTurn.accept(player);

		if (getSize.get() == 1)
                {
			next = endOfGamePhase.apply(player);
                }
                else
                {
		        next = playerRollsDicePhase.apply(player);
                }

                return next;
	}

	private Consumer1<Player> notifyNewTurn;
	private Supplier<Integer> getSize;
	private Supplier<Player> getNext;
	private Function1<Player, Phase> endOfGamePhase;
	private Function1<Player, Phase> playerRollsDicePhase;
}
