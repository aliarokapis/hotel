package hotel.phases;

import hotel.phases.Phase;
import hotel.Player;
import hotel.Hotel;
import hotel.util.function.*;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.List;
import java.util.function.*;

/**
 * Player has landed on a Build Tile Phase.
 * Uses functionl interfaces for dependency injection.
 * Checks if available hotel for building, if so asks the player, leads to PlayerChoseToBuildHotelPhase if so,
 * leads to next phase otherwise.
 */

public class PlayerOnBuildTilePhase implements Phase
{
	public PlayerOnBuildTilePhase(
		Player player,
		Supplier<Stream<Hotel>> getHotels,
		Function1<Hotel, Stream<Integer>> neighboringLocations,
		Predicate<Integer> entranceAtLocation,
		Consumer1<List<Hotel>> notifyPlayerAvailableHotelsForEntrance,
		Consumer1<List<Hotel>> notifyPlayerAvailableHotelsForUpgrade,
		Function2<Boolean, Boolean, Integer> getChoice,
		Function1<List<Hotel>, Phase> playerChoseToUpgradeHotelPhase,
		Function1<List<Hotel>, Phase> playerChoseToPutHotelEntrancePhase,
		Supplier<Phase> next
	)
	{
		this.player = player;
		this.getHotels = getHotels;
                this.neighboringLocations = neighboringLocations;
		this.entranceAtLocation = entranceAtLocation;
		this.notifyPlayerAvailableHotelsForEntrance = notifyPlayerAvailableHotelsForEntrance;
		this.notifyPlayerAvailableHotelsForUpgrade = notifyPlayerAvailableHotelsForUpgrade;
		this.getChoice = getChoice;
		this.playerChoseToUpgradeHotelPhase = playerChoseToUpgradeHotelPhase;
		this.playerChoseToPutHotelEntrancePhase = playerChoseToPutHotelEntrancePhase;
		this.next = next;
	}

	public Phase next()
	{
		List<Hotel> hotelsToPutEntranceChoices = 
			getHotels.get()
                                 .filter(h -> h.getOwner().filter(p -> p == player).isPresent())
				 .filter(h -> h.isBuilt())
                                 .filter(h -> 
                                        neighboringLocations.apply(h)
                                                .filter(l -> !entranceAtLocation.test(l))
                                                .findAny()
                                                .isPresent())
				 .collect(Collectors.toList());

		List<Hotel> hotelsToUpgradeChoices = 
			getHotels.get()
                                 .filter(h -> h.getOwner().filter(p -> p == player).isPresent())
                                 .filter(h -> h.canUpgrade())
				 .collect(Collectors.toList());

		boolean canPutEntrance = !hotelsToPutEntranceChoices.isEmpty();
		boolean canUpgrade = !hotelsToUpgradeChoices.isEmpty();

		if (!canPutEntrance && !canUpgrade)
			return next.get();

		if (canPutEntrance)
			notifyPlayerAvailableHotelsForEntrance.accept(hotelsToPutEntranceChoices);

		if (canUpgrade)
			notifyPlayerAvailableHotelsForUpgrade.accept(hotelsToUpgradeChoices);

		int choice = getChoice.apply(canPutEntrance, canUpgrade);

		if (choice == 1)
			return playerChoseToUpgradeHotelPhase.apply(hotelsToUpgradeChoices);

		if (choice == 2)
			return playerChoseToPutHotelEntrancePhase.apply(hotelsToPutEntranceChoices);

		return next.get();
	}

	private Player player;
	private Supplier<Stream<Hotel>> getHotels;
	private Function1<Hotel, Stream<Integer>> neighboringLocations;
	private Predicate<Integer> entranceAtLocation;
	private Consumer1<List<Hotel>> notifyPlayerAvailableHotelsForEntrance;
	private Consumer1<List<Hotel>> notifyPlayerAvailableHotelsForUpgrade;
	private Function2<Boolean, Boolean, Integer> getChoice;
	private Function1<List<Hotel>, Phase> playerChoseToUpgradeHotelPhase;
	private Function1<List<Hotel>, Phase> playerChoseToPutHotelEntrancePhase;
	private Supplier<Phase> next;
}

