package hotel.phases;

/** 
 * Interface representing a Phase of the game.
 * It is practically isomorphic to a Runnable but allows
 * recursive calls and makes chaining explicit.
 */

public interface Phase
{
	Phase next();
}


