package hotel.phases;

import hotel.phases.Phase;
import hotel.Tile;
import hotel.Player;
import hotel.util.function.*;
import java.util.function.*;

/** 
 * Player Is Moving Phase.
 * Uses functional interfaces for dependency injection.
 * Gets tile and leads to PlayePassesThroughPhase reducing the number of remaining moves by one.
 */

public class PlayerIsMovingPhase implements Phase
{
	public PlayerIsMovingPhase(
		Player player, 
		Integer remainingMoves,
		Supplier<Integer> getLocation,
		Function2<Integer, Integer, Integer> nextLocation,
		Consumer1<Integer> movePlayer,
		Supplier<Tile> getTileBelow,
		Function2<Tile, Integer, Phase> playerPassesThroughPhase
	)
	{
		this.player = player;
		this.remainingMoves = remainingMoves;
		this.getLocation = getLocation;
		this.nextLocation = nextLocation;
		this.movePlayer = movePlayer;
		this.getTileBelow = getTileBelow;
		this.playerPassesThroughPhase = playerPassesThroughPhase;
	}

	public Phase next()
	{
		Integer location = getLocation.get();

		Integer next = nextLocation.apply(location, 1);

		movePlayer.accept(next);

		Tile tile = getTileBelow.get();

		return playerPassesThroughPhase.apply(tile, remainingMoves - 1);
	}

	private Player player; 
	private Integer remainingMoves;
	private Supplier<Integer> getLocation;
	private Function2<Integer, Integer, Integer> nextLocation;
	private Consumer1<Integer> movePlayer;
	private Supplier<Tile> getTileBelow;
	private Function2<Tile, Integer, Phase> playerPassesThroughPhase;
}

