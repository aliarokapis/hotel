package hotel.phases;

import hotel.Tile;
import hotel.util.function.*;
import hotel.phases.Phase;
import hotel.Player;

/** 
 * Player Passes Through Tile Phase.
 * Uses functional interfaces for dependency injection.
 * Leads to PassesThroughTownHall Phase, PassesThroughBank Phase if passing through the appropriate tile
 * or leads to PlayerDonePassingThrough Phase if not passing through either.
 */

public class PlayerPassesThroughPhase implements Phase
{
	public PlayerPassesThroughPhase(
		Player player,
		Tile tile,
		Integer remainingMoves,
		Function3<Player, Tile, Integer, Phase> playerPassesThroughBankTilePhase,
		Function3<Player, Tile, Integer, Phase> playerPassesThroughTownHallTilePhase,
		Function3<Player, Tile, Integer, Phase> playerDonePassingThroughPhase
        )
	{
		this.player = player;
		this.tile = tile;
		this.remainingMoves = remainingMoves;
		this.playerPassesThroughBankTilePhase = playerPassesThroughBankTilePhase;
		this.playerPassesThroughTownHallTilePhase = playerPassesThroughTownHallTilePhase;
		this.playerDonePassingThroughPhase = playerDonePassingThroughPhase;
	}

	public Phase next()
	{
		switch (tile)
		{
		case BANK:
			return playerPassesThroughBankTilePhase.apply(player, tile, remainingMoves);
		case TOWN_HALL:
			return playerPassesThroughTownHallTilePhase.apply(player, tile, remainingMoves);
		}

		return playerDonePassingThroughPhase.apply(player, tile, remainingMoves);
	}

	private Player player;
	private Tile tile;
	private Integer remainingMoves;
	private Function3<Player, Tile, Integer, Phase> playerPassesThroughBankTilePhase;
	private Function3<Player, Tile, Integer, Phase> playerPassesThroughTownHallTilePhase;
	private Function3<Player, Tile, Integer, Phase> playerDonePassingThroughPhase;
}
