package hotel.phases;

import hotel.Player;
import hotel.Hotel;
import hotel.phases.Phase;
import hotel.util.function.*;
import java.util.function.*;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.List;
import java.util.Optional;

/**
 * Player Chose to Upgrade Hotel Phase.
 * Uses functional interfaces for dependency injection.
 * Checks if player chose Hotel, if so performs upgrade logic, otherwise leads to Retry phase.
 * Leads to next phase afterwards.
 */

public class PlayerChoseToUpgradeHotelPhase implements Phase
{
	public PlayerChoseToUpgradeHotelPhase(
		Player player,
		List<Hotel> hotelsToUpgradeChoices,
                Supplier<Optional<Hotel>> chooseHotel,
		Supplier<Integer> applianceResponse,
		Consumer1<Hotel> notifyPlayerNormalBuildingApproved,
		Consumer1<Hotel> notifyPlayerBuildingNotApproved,
		Consumer1<Hotel> notifyPlayerFreeBuildingApproved,
		Consumer1<Hotel> notifyPlayerOverCostBuildingApproved,
                Supplier<Phase> retry,
		Supplier<Phase> next
	)
	{
		this.player = player;
		this.hotelsToUpgradeChoices = hotelsToUpgradeChoices;
		this.chooseHotel = chooseHotel;
		this.applianceResponse = applianceResponse;
		this.notifyPlayerNormalBuildingApproved = notifyPlayerNormalBuildingApproved;
		this.notifyPlayerBuildingNotApproved = notifyPlayerBuildingNotApproved;
		this.notifyPlayerFreeBuildingApproved = notifyPlayerFreeBuildingApproved;
		this.notifyPlayerOverCostBuildingApproved = notifyPlayerOverCostBuildingApproved;
                this.retry = retry;
		this.next = next;
	}

	public Phase next()
	{
		Optional<Hotel> mhotel = chooseHotel.get();

                if (!mhotel.isPresent())
                        return retry.get();

                Hotel hotel = mhotel.get();

		int result = applianceResponse.get();

		if (result == 0)	
		{
			notifyPlayerNormalBuildingApproved.accept(hotel);
			player.setBalance(player.getBalance() - hotel.getBuildingCost());
			hotel.upgrade();
		}
		else if (result == 1)
		{
			notifyPlayerBuildingNotApproved.accept(hotel);
		}
		else if (result == 2)
		{
			notifyPlayerFreeBuildingApproved.accept(hotel);
			hotel.upgrade();
		}
		else 
		{
			notifyPlayerOverCostBuildingApproved.accept(hotel);
			player.setBalance(player.getBalance() - 2 * hotel.getBuildingCost());
			hotel.upgrade();
		}

		return next.get();
	}

	private Player player;
	private List<Hotel> hotelsToUpgradeChoices;
        private Supplier<Optional<Hotel>> chooseHotel;
	private Supplier<Integer> applianceResponse;
	private Consumer1<Hotel> notifyPlayerNormalBuildingApproved;
	private Consumer1<Hotel> notifyPlayerBuildingNotApproved;
	private Consumer1<Hotel> notifyPlayerFreeBuildingApproved;
	private Consumer1<Hotel> notifyPlayerOverCostBuildingApproved;
        private Supplier<Phase> retry;
	private Supplier<Phase> next;
}
