package hotel.phases;

import hotel.Tile;
import hotel.util.function.*;
import java.util.function.*;
import hotel.phases.Phase;
import hotel.Player;

/** 
 * Player Passes Through Bank Tile Phase.
 * Uses functional interfaces for dependency injection.
 * Bank pays player and leads the game to the next phase.
 */

public class PlayerPassesThroughBankPhase implements Phase
{
	public PlayerPassesThroughBankPhase(
		Player player,
		Consumer1<Integer> notifyPlayerBankPayment,
		Supplier<Phase> next
	)
	{
		this.player = player;
		this.notifyPlayerBankPayment = notifyPlayerBankPayment;
		this.next = next;
	}

	public Phase next()
	{
		Integer payment = 1000;
		notifyPlayerBankPayment.accept(payment);
		player.setBalance(player.getBalance() + payment);

		return next.get();
	}

	private Player player;
	private Consumer1<Integer> notifyPlayerBankPayment;
	private Supplier<Phase> next;
}
