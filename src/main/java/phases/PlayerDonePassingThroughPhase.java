package hotel.phases;

import hotel.Tile;
import hotel.util.function.*;
import hotel.phases.Phase;
import hotel.Player;

/**
 * Player Is Done Passing Through Tile Phase.
 * Uses functional interfaces for dependency injection.
 * Checks remaining moves and leads to the PlayerHandleEntrancePhase if non remaining,
 * Leads to playerIsMovingPhase otherwise.
 */

public class PlayerDonePassingThroughPhase implements Phase
{
	public PlayerDonePassingThroughPhase (
		Player player,
		Tile tile,
		Integer remainingMoves,
		Function2<Player, Integer, Phase> playerIsMovingPhase,
		Function2<Player, Tile, Phase> playerHandleEntrancePhase
        )
	{
		this.player = player;
		this.tile = tile;
		this.remainingMoves = remainingMoves;
		this.playerIsMovingPhase = playerIsMovingPhase;
		this.playerHandleEntrancePhase = playerHandleEntrancePhase;
	}

	public Phase next()
	{
		if (remainingMoves == 0)
			return playerHandleEntrancePhase.apply(player, tile);

		return playerIsMovingPhase.apply(player, remainingMoves);
	}

	private Player player;
	private Tile tile;
	private Integer remainingMoves;
	private Function2<Player, Integer, Phase> playerIsMovingPhase;
	private Function2<Player, Tile, Phase> playerHandleEntrancePhase;
}
