package hotel.phases;

import hotel.Player;
import hotel.Hotel;
import hotel.phases.Phase;
import hotel.util.function.*;
import java.util.function.*;
import java.util.Optional;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;

/**
 * Player Chose to Purchase Hotel Phase.
 * Uses functional interfaces for dependency injection.
 * Checks if player chose Hotel, if so performs purchase logic, otherwise leads to Retry phase.
 * Leads to next phase afterwards.
 */

public class PlayerChoseToPurchaseHotelPhase implements Phase
{
	public PlayerChoseToPurchaseHotelPhase(
		Player player,
		List<Hotel> hotelsAvailableForPurchase,
		Supplier<Optional<Hotel>> chooseHotel,
		Consumer1<Hotel> notifyPlayerPurchase,
		Consumer2<Player, Hotel> notifyOwnerRepurchase,
		Supplier<Phase> retry,
		Supplier<Phase> next
	)
	{
		this.player = player;
		this.hotelsAvailableForPurchase = hotelsAvailableForPurchase;
		this.chooseHotel = chooseHotel;
		this.notifyPlayerPurchase = notifyPlayerPurchase;
		this.notifyOwnerRepurchase = notifyOwnerRepurchase;
		this.retry = retry;
		this.next = next;
	}

	public Phase next()
	{
		Optional<Hotel> mhotel = chooseHotel.get();

		if (!mhotel.isPresent())
			return retry.get();

		Hotel hotel = mhotel.get();
	
		Optional<Player> mowner = hotel.getOwner();

		if (mowner.isPresent())
		{
			Player owner = mowner.get();

			notifyOwnerRepurchase.accept(owner, hotel);
			Integer price = hotel.getForcedPurchasePrice();
			player.setBalance(player.getBalance() - price);
			owner.setBalance(owner.getBalance() + price);
		}
		else
		{
			notifyPlayerPurchase.accept(hotel);
			Integer price = hotel.getFreePurchasePrice();
			player.setBalance(player.getBalance() - price);
		}

		hotel.setOwner(player);

		return next.get();
	}

	private Player player;
	private List<Hotel> hotelsAvailableForPurchase;
	private Supplier<Optional<Hotel>> chooseHotel;
	private Consumer1<Hotel> notifyPlayerPurchase;
	private Consumer2<Player, Hotel> notifyOwnerRepurchase;
	private Supplier<Phase> retry;
        private Supplier<Phase> next;
}
