package hotel.phases;

import java.util.function.*;
import java.util.Optional;
import hotel.util.function.*;
import hotel.Player;
import hotel.Hotel;
import hotel.phases.Phase;

/**
 * Player Handles Entrance Phase.
 * Uses functional interfaces for dependency injection.
 * Checks if the player must pay another player in case of an entrance on the player's landing tile.
 * Leads to next phase when done.
 */

public class PlayerHandleEntrancePhase implements Phase
{
	public PlayerHandleEntrancePhase(
		Player player,	
                Function1<Integer, Optional<Hotel>> getEntrance,
		Supplier<Integer> dice,
		Consumer2<Player, Integer> notifyPlayerPayment,
		Runnable bankruptcyHandler,
		Supplier<Phase> onBankrupcy,
		Supplier<Phase> next
	)
	{
		this.player = player;
		this.getEntrance = getEntrance;
		this.dice = dice;
		this.notifyPlayerPayment = notifyPlayerPayment;
		this.bankruptcyHandler = bankruptcyHandler;
		this.onBankrupcy = onBankrupcy;
		this.next = next;
	}

	public Phase next()
	{
                Optional<Hotel> mentrance = getEntrance.apply(player.getLocation());

                if (!mentrance.isPresent())
                        return next.get();

                Hotel hotel = mentrance.get();
                
		if (hotel.getBoughtByBank())
			return next.get();

		Optional<Player> mowner = hotel.getOwner();

		if (!mowner.isPresent())
			return next.get();

		Player owner = mowner.get();

		if (owner != player)
		{
			Integer daysToStay = dice.get();
			Integer stayPrice = daysToStay * hotel.getChargingPrice();

			notifyPlayerPayment.accept(owner, stayPrice);

			owner.setBalance(owner.getBalance() + stayPrice);

                        player.setBalance(player.getBalance() - stayPrice);

			if (player.getBalance() <= 0)
			{
				bankruptcyHandler.run();
				return onBankrupcy.get();
			}
		}

		return next.get();
	}

	private Player player;	
        private Function1<Integer, Optional<Hotel>> getEntrance;
	private Supplier<Integer> dice;
	private Consumer2<Player, Integer> notifyPlayerPayment;
	private Runnable bankruptcyHandler;
	private Supplier<Phase> onBankrupcy;
	private Supplier<Phase> next;
}
