package hotel.phases;

import hotel.Tile;
import hotel.util.function.*;
import java.util.function.*;
import hotel.phases.Phase;
import hotel.Player;

/**
 * Player Done Paying Entrance And Moved To Tile Phase.
 * Uses functional interfaces for dependency injection.
 * If the player has landed to a PurchaseTile leads to PlayerOnPurchaseTilePhase.
 * If the player has landed to a BuildTile leads to PlayerOnBuildTilePhase.
 * Returns next phase otherwise.
 */

public class PlayerMovedToPhase implements Phase
{
	public PlayerMovedToPhase(
		Player player,
		Tile tile,
		Supplier<Phase> playerOnPurchaseTilePhase,
		Supplier<Phase> playerOnBuildTilePhase,
		Supplier<Phase> next
	)
	{
		this.player = player;
		this.tile = tile;
		this.playerOnPurchaseTilePhase = playerOnPurchaseTilePhase;
		this.playerOnBuildTilePhase = playerOnBuildTilePhase;
		this.next = next;
	}

	public Phase next()
	{
		switch (tile)
		{
		case BUILD:
			return playerOnBuildTilePhase.get();
		case PURCHASE:
			return playerOnPurchaseTilePhase.get();
		}

		return next.get();
	}

	private Player player;
	private Tile tile;
	private Supplier<Phase> playerOnPurchaseTilePhase;
	private Supplier<Phase> playerOnBuildTilePhase;
	private Supplier<Phase> next;
}
