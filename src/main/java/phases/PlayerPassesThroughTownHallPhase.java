package hotel.phases;

import hotel.Player;
import hotel.Hotel;
import hotel.phases.Phase;
import hotel.util.function.*;
import java.util.function.*;
import java.util.Optional;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;

/**
 * Player Passes Through Town Hall Tile Phase.
 * Uses functional interfaces for dependency injection.
 * Checks if the player has the option to put an entrances and if so asks the player
 * moving to the appropriate phase if he accepts.
 * Leads to the next phase otherwise.
 */

public class PlayerPassesThroughTownHallPhase implements Phase
{
	public PlayerPassesThroughTownHallPhase(
		Player player,
		Supplier<Stream<Hotel>> getHotels,
		Function1<Hotel, Stream<Integer>> neighboringLocations,
		Predicate<Integer> entranceAtLocation,
		Consumer1<List<Hotel>> notifyPlayerAvailableHotelsForEntrance,
		Supplier<Boolean> choseToPutEntrance,
		Function1<List<Hotel>, Phase> playerChoseToPutEntrancePhase,
		Supplier<Phase> next
	)
	{
		this.player = player;
		this.getHotels = getHotels;
		this.neighboringLocations = neighboringLocations;
		this.entranceAtLocation = entranceAtLocation;
		this.notifyPlayerAvailableHotelsForEntrance = notifyPlayerAvailableHotelsForEntrance;
		this.choseToPutEntrance = choseToPutEntrance;
		this.playerChoseToPutEntrancePhase = playerChoseToPutEntrancePhase;
		this.next = next;
	}

	public Phase next()
	{
		List<Hotel> hotelsToPutEntranceChoices = 
			getHotels.get()
                                 .filter(h -> h.getOwner().filter(p -> p == player).isPresent())
				 .filter(h -> h.isBuilt())
                                 .filter(h -> 
                                        neighboringLocations.apply(h)
                                                .filter(l -> !entranceAtLocation.test(l))
                                                .findAny()
                                                .isPresent())
				 .collect(Collectors.toList());

		if (hotelsToPutEntranceChoices.isEmpty())
			return next.get();

		notifyPlayerAvailableHotelsForEntrance.accept(hotelsToPutEntranceChoices);

		if (choseToPutEntrance.get())
		{
			return playerChoseToPutEntrancePhase.apply(hotelsToPutEntranceChoices);
		}

		return next.get();
	}

	private Player player;
	private Supplier<Stream<Hotel>> getHotels;
	private Function1<Hotel, Stream<Integer>> neighboringLocations;
	private Predicate<Integer> entranceAtLocation;
	private Consumer1<List<Hotel>> notifyPlayerAvailableHotelsForEntrance;
	private Supplier<Boolean> choseToPutEntrance;
	private Function1<List<Hotel>, Phase> playerChoseToPutEntrancePhase;
	private Supplier<Phase> next;
}
