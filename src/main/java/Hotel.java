package hotel;

import java.util.List;
import java.util.ArrayList;
import hotel.Player;
import java.util.Optional;
import javafx.beans.property.*;
import java.util.Collections;

/** 
 * Class representing a Hotel.
 * Provides read-only getters for non-changing attributes and
 * JavaFx properties for the attributes that can change.
 */

public class Hotel
{
	/** 
	 * Helper class used to represent Level-specific upgrade cost and entrance charging price.
	 */
	static public class BuildingCostAndChargingPrice
	{
		/** 
	  	 * Constructor.
		 * @param buildingCost The cost that is required in order to upgrade/unlock the specific level.
		 * @param chargingPrice The base charging price per night that the players moving to an entrance tile must pay.
		 */
		public BuildingCostAndChargingPrice(Integer buildingCost, Integer chargingPrice)
		{
			this.buildingCost = buildingCost;
			this.chargingPrice = chargingPrice;
		}

		/** 
	  	 * Gets the building cost.
		 * @return The building cost.
		 */
                public Integer getBuildingCost()
                {
                        return buildingCost;
                }

		/** 
	  	 * Gets the charging price.
		 * @return The charging price.
		 */
                public Integer getChargingPrice()
                {
                        return chargingPrice;
                }

		private Integer buildingCost;
		private Integer chargingPrice;
	};

	/**
	 * Constructor.
	 * @param id The id of the Hotel.
	 * @param name The name of the Hotel.
	 * @param freePurchasePrice The price that is required in order to purchase the Hotel if it has no current Owner.
	 * @param forcedPurchasePrice The price that is required in order to purchase the Hotel if it has a current Owner.
	 * @param buildEntranceCost The price that is required to build an Entrance for this Hotel.
	 * @param prices A List of building cost and charging prices per level of the Hotel.
	 */
	public Hotel(
		Integer id,
		String name,
		Integer freePurchasePrice,
		Integer forcedPurchasePrice,
                Integer buildEntranceCost,
		List<BuildingCostAndChargingPrice> prices)
	{
		this.id = id;
		this.name = name;
		this.freePurchasePrice = freePurchasePrice;
		this.forcedPurchasePrice = forcedPurchasePrice;
                this.buildEntranceCost = buildEntranceCost;
		this.prices = new ArrayList<>(prices);

		this.level = new SimpleIntegerProperty(0);
		this.owner = new SimpleObjectProperty<Player>();
		this.boughtByBank = new SimpleBooleanProperty();
	}

	/**
	 * Checks whether the Hotel can be upgraded.
	 * @return true if can be upgraded, false otherwise.
	 */
	public boolean canUpgrade()
	{
		return level.getValue() < prices.size();
	}

	/**
	 * Upgrades the Hotel. Does not perform any checking.
	 */
	public void upgrade()
	{
		level.setValue(level.getValue()+1);
	}

	/**
	 * Checks whether the Hotel is built.
	 * @return true if the Hotel is built, false otherwise.
	 */
	public boolean isBuilt()
	{
		return getLevel() > 0;
	}

	/**
	 * Gets the price that is required in order to purchase the Hotel if it has no current Owner.
	 * @return The price that is required in order to purchase the Hotel if it has no current Owner.
	 */
	public Integer getFreePurchasePrice()
	{
		return freePurchasePrice;
	}

	/**
	 * Gets the price that is required in order to purchase the Hotel if it has a current Owner.
	 * @return The price that is required in order to purchase the Hotel if it has a current Owner.
	 */
	public Integer getForcedPurchasePrice()
	{
		return forcedPurchasePrice;
	}

	/**
	 * Gets the price that is required to build an Entrance for this Hotel.
	 * @return The price that is required to build an Entrance for this Hotel.
	 */
        public Integer getBuildEntranceCost()
        {
                return buildEntranceCost;
        }

	/**
	 * Gets the building cost that is required in order to upgrade the Hotel to the next level.
	 * @return The building cost that is required in order to upgrade the Hotel to the next level.
	 */
	public Integer getBuildingCost()
	{
		return prices.get(getLevel()).getBuildingCost();
	}

	/**
	 * Gets the price that Players are charged per night stayed at the Hotel.
	 * @return The price that Players are charged per night stayed at the Hotel.
	 */
	public Integer getChargingPrice()
	{
		return prices.get(getLevel()-1).chargingPrice;
	}

	/**
	 * Gets the name of the Hotel.
	 * @return The name of the Hotel.
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Gets the id of the Hotel.
	 * @return The id of the Hotel.
	 */
	public Integer getId()
	{
		return id;
	}

	/**
	 * Sets the Owner of the Hotel.
	 * @param v The new Owner of the Hotel.
	 */
	public void setOwner(Player v)
	{
		owner.setValue(v);
	}

	/**
	 * Gets the Owner of the Hotel if the Hotel has an owner.
	 * @return A non-empty Optional if the Hotel has an owner, an empty Optional otherwise.
	 */
	public Optional<Player> getOwner()
	{
		return Optional.ofNullable(owner.getValue());
	}

	/**
	 * Gets the Owner property of the Hotel.
	 * @return The Owner property of the Hotel.
	 */
        public ObjectProperty<Player> getOwnerProperty()
        {
                return owner;
        }

	/**
	 * Sets the bought-by-bank state of the Hotel.
	 * @param v Whether the Hotel is bought by the bank.
	 */
	public void setBoughtByBank(boolean v)
	{
		boughtByBank.setValue(v);
	}

	/**
	 * Gets the bought-by-bank state of the Hotel.
	 * @return Whether the Hotel is bought by the bank.
	 */
	public Boolean getBoughtByBank()
	{
		return boughtByBank.getValue();	
	}

	/**
	 * Gets the bought-by-bank property of the Hotel.
	 * @return The bought-by-bank property of the Hotel.
	 */
        public BooleanProperty getBoughtByBankProperty()
        {
                return boughtByBank;
        }

	/**
	 * Gets the current Level of the Hotel.
	 * @return The current Level of the Hotel.
	 */
        public Integer getLevel()
        {
                return level.getValue();
        }

	/**
	 * Sets the current Level of the Hotel.
	 * @param v The new level.
	 */
        public void setLevel(int v)
        {
               level.setValue(v);
        }

	 /**
	 * Gets the level property of the Hotel.
	 * @return The Level property of the Hotel.
	 */
        public IntegerProperty getLevelProperty()
        {
               return level;
        }

	/**
	 * Gets an unmodifiable List of the building costs and charging prices of each level.
	 * @return An unmodifiable List of the building costs and charging prices of each level.
	 */
        public List<BuildingCostAndChargingPrice> getPrices()
        {
                return Collections.unmodifiableList(prices);
        }

	private Integer id;
	private String name;
        private Integer buildEntranceCost;
	private Integer freePurchasePrice;
	private Integer forcedPurchasePrice;
	private List<BuildingCostAndChargingPrice> prices;

	private ObjectProperty<Player> owner;
	private IntegerProperty level;
	private BooleanProperty boughtByBank;
}
