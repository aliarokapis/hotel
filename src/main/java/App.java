package hotel;

import javafx.application.Application;
import javafx.stage.Screen;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
import javafx.concurrent.Task;
import java.net.URL;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import java.util.Map;
import java.util.Collections;
import java.util.stream.Stream;
import java.util.concurrent.CompletableFuture;
import java.io.File;
import java.nio.file.*;
import java.util.stream.Collectors;

import hotel.Board;
import hotel.util.loaders.FileLoader2d;
import hotel.util.function.*;
import hotel.ui.UIController;
import hotel.Hotel;
import hotel.util.creators.*;

/** 
 * The class providing the main function.
 * Initiates the JavafX thread and the game loop.
 */
public class App extends Application {

    /**
     * The main function.
     * @param args The application's arguments.
     */
    public static void main(String[] args) {
        launch(args);
    }

    private static String getNameWithoutExtension(Path path) {
	Path name = path.getFileName();
	if (name == null) {
	    return "";
	}
	String fileName = name.toString();
	int dotIndex = fileName.lastIndexOf('.');
	return dotIndex == -1 ? fileName : fileName.substring(0, dotIndex);
    }

    /**
     * This function retrieves the description files, builds the GameState object
     * and the UI controller, launches the main window and initiates the Game's gameloop task.
     *
     * @param primaryStage JavaFX entry point.
     */

    @Override
    public void start(Stage primaryStage) throws Exception {

	List<Path> boards = Files.walk(Paths.get(getClass().getResource("/boards/").toURI())).filter(Files::isDirectory).collect(Collectors.toList());
	boards.remove(0);

	Collections.shuffle(boards);

	for (Path p : boards)
	{
		System.out.println(p.toString());
	}


	Path level = boards.get(0);

	Map<Boolean, List<Path>> files = Files.walk(level).filter(Files::isRegularFile).collect(Collectors.partitioningBy(s -> s.endsWith("board.txt")));

	Path boardPath = files.get(true).get(0);
	List<Path> hotelPaths = files.get(false);

	List<List<String>> boardData = FileLoader2d.load(boardPath.toFile());
	Board<String> board = new Board<>(boardData);

	List<Board.Coordinates> path = PathCreator.create(board, Set.of("S", "B", "C", "E", "H"), "S");
	List<Tile> tiles = TileCreator.create(board, path);

	List<Player> players = new ArrayList();
	players.add(new Player("Player 1", 0, 12000));
	players.add(new Player("Player 2", 0, 12000));
	players.add(new Player("Player 3", 0, 12000));


	
	List<Hotel> hotels = new ArrayList();

	for (Path hpath : hotelPaths)
	{
		List<List<String>> hotelData = FileLoader2d.load(hpath.toFile());
		Integer id = Integer.valueOf(getNameWithoutExtension(hpath));
		hotels.add(HotelCreator.create(id, hotelData));
	}

	GameState state = new GameState(players, tiles, hotels, path, board);

	UIController controller = new UIController(state);

	FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ui.fxml"));
	loader.setController(controller);

	Parent root = loader.load();

        primaryStage.setTitle("MediaLab Hotel");
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
	primaryStage.setWidth(Screen.getPrimary().getVisualBounds().getWidth());
	primaryStage.setHeight(Screen.getPrimary().getVisualBounds().getHeight());
	primaryStage.setMaximized(true);

        Task<Void> task = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                        Game game = new Game(state, controller);
                        game.run();
                        return null;
                }
        };

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();

        primaryStage.show();

    }
}


