package hotel.util.creators;

import hotel.Tile;
import hotel.Board;
import hotel.util.creators.PathCreator;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;

/**
 * Creates a List of Tiles given a Board.Coordinates path and a Board<String>.
 * Uses the identifiers specified in the specification to build each Tile.
 */

public class TileCreator
{
	static public List<Tile> create(Board<String> board, List<Board.Coordinates> path)
	{
		List<Tile> res = new ArrayList();

		for (Board.Coordinates coords : path)
		{
			switch (board.getPieceAt(coords.x, coords.y))
			{
			case "S":
				res.add(Tile.START);
				break;		
			case "F":
				res.add(Tile.FREE);
				break;		
			case "E":
				res.add(Tile.BUILD);
				break;		
			case "B":
				res.add(Tile.BANK);
				break;		
			case "H":
				res.add(Tile.PURCHASE);
				break;		
			case "C":
				res.add(Tile.TOWN_HALL);
				break;		
			default:
			}
		}

		return res;
	}
}
