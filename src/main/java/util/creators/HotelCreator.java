package hotel.util.creators;

import hotel.Hotel;
import java.util.List;
import java.util.ArrayList;

/** 
 * Creates a Hotel given a 2d List of Strings.
 */

public class HotelCreator
{
	static public Hotel create(Integer id, List<List<String>> raw)
	{
		String name = raw.get(0).get(0);
		Integer freePrice = Integer.valueOf(raw.get(1).get(0));
		Integer forcedPrice = Integer.valueOf(raw.get(1).get(1));
		Integer buildEntranceCost = Integer.valueOf(raw.get(2).get(0));

		List<Hotel.BuildingCostAndChargingPrice> prices = new ArrayList<>();

		for (int i = 3; i != raw.size(); ++i)
		{
			Integer cost = Integer.valueOf(raw.get(i).get(0));
			Integer charge = Integer.valueOf(raw.get(i).get(1));
			prices.add(new Hotel.BuildingCostAndChargingPrice(cost, charge));
		}

		return new Hotel(id, name, freePrice, forcedPrice, buildEntranceCost, prices);
	}
}
