package hotel.util.creators;

import hotel.Board;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.Optional;
import hotel.util.function.Function4;

/**
 * Creates a List of Coordinates given a Board, a set of PathTiles and a Start symbol.
 * The process requires that there is a single cycle leading from and to the Start symbol.
 * The path is created in an anti-clockwise direction.
 */

public class PathCreator
{
	
	public static<T> List<Board.Coordinates> create(Board<T> board, Set<T> pathTiles, T start)
	{
		List<Board.Coordinates> coords = board.getCoordinatesOfPiece(start);

		if (coords.size() != 1)
			throw new IllegalArgumentException("There should only be a single start piece");

		Board.Coordinates curCoords = coords.get(0);
		Board.Coordinates prev = curCoords;

		ArrayList<Board.Coordinates> res = new ArrayList<Board.Coordinates>();

	        Function4<Integer, Integer, Integer, Integer, Boolean> genericNextValid = (x, y, px, py) -> 
                {
                    if (!board.areValidCoordinates(x, y))
                        return false;

                    if (x == px && y == py)
                        return false;

                    if (board.getPieceAt(x, y).equals(start))
                        return false;

                    if (!pathTiles.contains(board.getPieceAt(x, y)))
                        return false;

                    return true;
                };

		while (true)
		{
			res.add(curCoords);

			Board.Coordinates next = null;

                        if (genericNextValid.apply(curCoords.x+1, curCoords.y, prev.x, prev.y))
                        {
                                next = new Board.Coordinates(curCoords.x+1, curCoords.y);
                        }
                        else if (genericNextValid.apply(curCoords.x, curCoords.y-1, prev.x, prev.y))
                        {
                                next = new Board.Coordinates(curCoords.x, curCoords.y-1);
                        }
                        else if (genericNextValid.apply(curCoords.x-1, curCoords.y, prev.x, prev.y))
                        {
                                next = new Board.Coordinates(curCoords.x-1, curCoords.y);
                        }
                        else if (genericNextValid.apply(curCoords.x, curCoords.y+1, prev.x, prev.y))
                        {
                                next = new Board.Coordinates(curCoords.x, curCoords.y+1);
                        }

			if (next == null)
				break;

			prev = curCoords;
			curCoords = next;
		}

		return res;
	}
}
