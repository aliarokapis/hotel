package hotel.util.function;

@FunctionalInterface
public interface Function4<A, B, C, D, E> {
	E apply(A a, B b, C c, D e);
}

