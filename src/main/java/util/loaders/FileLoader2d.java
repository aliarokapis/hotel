package hotel.util.loaders;

import java.util.List;
import java.util.ArrayList;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;

public class FileLoader2d
{
	static public List<List<String>> load(File file) throws FileNotFoundException, IOException
	{
		FileReader fileReader = new FileReader(file);
		BufferedReader reader = new BufferedReader(fileReader);

		List<List<String>> raw = new ArrayList<List<String>>();

 		String line = null;

		while ((line = reader.readLine()) != null)
		{
			String[] splitted = line.replaceAll("\\s","").split(",");
			List<String> row = new ArrayList<String>();
			
			for (int i = 0; i != splitted.length; ++i)
			{
				row.add(splitted[i]);
			}

			raw.add(row);
		}

		return raw;
	}
}

