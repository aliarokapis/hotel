package hotel;

import javafx.beans.property.*;

/** 
 * Class representing a Player.
 */
public class Player
{
	/**
	 * Constructor.
	 * @param name The player's name.
	 * @param location The player's starting location.
	 * @param balance The player's starting balance.
	 */
	public Player(String name, Integer location, Integer balance)
	{
		this.name = name;
		this.location = new SimpleIntegerProperty(location);
		this.balance = new SimpleIntegerProperty(balance);
	}

	/**
	 * @return The player's name.
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return The player's location property.
	 */
	public IntegerProperty getLocationProperty()
	{
		return location;
	}

	/**
	 * @return The player's location.
	 */
	public Integer getLocation()
	{
		return location.getValue();
	}

	/**
	 * Sets the player's location.
	 * @param location The new location.
	 */
	public void setLocation(Integer location)
	{
		this.location.setValue(location);
	}

	/**
	 * @return The player's balance property.
	 */
	public IntegerProperty getBalanceProperty()
	{
		return balance;
	}

	/**
	 * @return The player's balance.
	 */
	public Integer getBalance()
	{
		return balance.getValue();
	}

	/**
	 * Sets the player's balance.
	 * @param balance The new balance.
	 */
	public void setBalance(Integer balance)
	{
		this.balance.setValue(balance);
	}

	private String name;
	private IntegerProperty location;
	private IntegerProperty balance;
	
}
