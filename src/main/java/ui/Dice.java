package hotel.ui;
import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/** 
 * Class representing a Dice's sprite.
 */
public class Dice
{
    private ImageView imageView;
    private int width = 128;
    private int height = 128;
    private int index = 0;
    private int result = 2;

    private static int[] FACE1 = new int[]{0, 0};
    private static int[] FACE2 = new int[]{1, 0};
    private static int[] FACE3 = new int[]{2, 0};
    private static int[] FACE4 = new int[]{3, 0};
    private static int[] FACE5 = new int[]{4, 0};
    private static int[] FACE6 = new int[]{5, 0};

    private int[][] faces = new int[][]{FACE1, FACE2, FACE3, FACE4, FACE5, FACE6};

    /** 
     * Constructor.
     */
    public Dice() {
        this.imageView = new ImageView(new Image(getClass().getResource("/assets/dice.png").toString()));
        this.imageView.setViewport(getBounds());
    }

    private Rectangle2D getBounds() {
        return new Rectangle2D(faces[index][0] * width, faces[index][1] * height, width, height);
    }


    /**
     * Sets the dice's face.
     * @param result The number to change the dice to.
     */
    public void rollDice(int result) {
        index = result-1;
        this.imageView.setViewport(getBounds());
    }

    /**
     * @return The ImageView of the dice.
     */
    public ImageView getImageView() {
        return imageView;
    }

}
