package hotel.ui;

import hotel.Player;
import hotel.Hotel;
import hotel.util.function.*;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.CompletableFuture;

/** 
 * An interface that provides visualization hooks to the Game's state machine.
 */

public interface UI
{
	/** 
	 * Called whenever a new player's turn is starting.
	 * Opportuniy for visualization.
	 * @param player The player.
	 */

	void newTurn(Player player);

	/**
	 * Provides the Dice result. 
	 * Opportunity for visualization.
	 * @param random A Random possibly used to supply random numbers.
	 * @return The dice result.
	 */
	Integer rollDice(Random random);

	/**
	 * Called when moving the player. 
	 * Opportunity for visualization.
	 * @param player The player.
	 * @param nextLocation The player's next location.
	 */
	void movePlayer(Player player, Integer nextLocation);

	/**
	 * Called when player is paid by bank.
	 * Opportunity for visualization.
	 * @param player The player.
	 * @param payment The bank's payment to the player.
	 */
	void playerIsPaidByBank(Player player, Integer payment);

	/**
	 * Called when showing available hotels to put entrance for.
	 * Opportuniy for visualization.
	 * @param player The player.
	 * @param hotels The available hotels to put entrance for.
	 */
	void showAvailableHotelsForEntrance(Player player, List<Hotel> hotels);

	/**
	 * Called when showing available hotels for upgrade.
	 * Opportuniy for visualization.
	 * @param player The player.
	 * @param hotels The available hotels for upgrade.
	 */
	void showAvailableHotelsForUpgrade(Player player, List<Hotel> hotels);

	/**
	 * Called when showing available hotels for purchase.
	 * Opportuniy for visualization.
	 * @param player The player.
	 * @param hotels The available hotels for purchase.
	 */
	void showAvailableHotelsForPurchase(Player player, List<Hotel> hotels);

	/**
	 * Provides the choice of the player on whether to put an entrance.
	 * Opportunity for choice visualization.
	 * @param player The player
	 * @return The player's choice.
	 */
	Boolean chooseToPutEntrance(Player player);

	/**
	 * Provides the choice of the player on whether to put an entrance.
	 * Opportunity for choice visualization.
	 * @param player The player
	 * @return The player's choice.
	 */
	Boolean chooseToPurchase(Player player);
	
	/**
	 * Provides the chosen Hotel. The interface allows the player to Choose no hotel.
	 * Opportunity for selection visualization.
	 * @param player The player
	 * @param hotels The hotels available for selection.
	 * @return An optional containing the player's chosen Hotel or lack thereof.
	 */
	Optional<Hotel> chooseHotel(Player player, List<Hotel> hotels);

	/**
	 * Provides the choice of the player on whether to put an entrance, upgrade a hotel or neither.
	 * Opportunity for choice visualization.
	 * @param player The player
	 * @param entrance The player is allowed by the rules to put entrance.
	 * @param upgrade The player is allowed by the rules to upgrade.
	 * @return The player's choice. 1 for Upgrade, 2 for Entrance, Whatever else if neither.
	 */
	Integer chooseUpgradeEntranceOrBoth(Player player, Boolean entrance, Boolean upgrade);

	/**
	 * Called when player purchased a hotel.
	 * Opportuniy for visualization.
	 * @param player The player
	 * @param hotel The purchased hotel.
	 */
        void playerPurchased(Player player, Hotel hotel);

	/**
	 * Called when player purchased a hotel from another player.
	 * Opportuniy for visualization.
	 * @param player The player
	 * @param owner The purchased hotel's existing owner.
	 * @param hotel The purchased hotel.
	 */
        void playerRepurchasedFrom(Player player, Player owner, Hotel hotel);

	/**
	 * Called when player sets an entrance.
	 * Opportuniy for visualization.
	 * @param player The player
	 * @param location The chosen location.
	 * @param hotel The hotel of the entrance.
	 */
	void setEntrance(Player player, Integer location, Hotel hotel);

	/**
	 * Called when player pays another player on entrance.
	 * Opportuniy for visualization.
	 * @param player The player
	 * @param owner The player that owns the hotel's entrance.
	 * @param payment The amount that the player must pay the owner.
	 */
	void playerPayment(Player player, Player owner, Integer payment);

	/**
	 * Called when a player goes bankrupt.
	 * Opportuniy for visualization.
	 * @param player The player.
	 */
	void bankruptcy(Player player);

	/**
	 * Called when a building's upgrade is approved normally.
	 * Opportuniy for visualization.
	 * @param player The player.
	 * @param hotel The hotels to be upgraded.
	 */
	void normalBuildingApproved(Player player, Hotel hotel);

	/**
	 * Called when a building's upgrade is not approved.
	 * Opportuniy for visualization.
	 * @param player The player.
	 * @param hotel The hotels to be upgraded.
	 */
	void buildingNotApproved(Player player, Hotel hotel);

	/**
	 * Called when a building's upgrade is approved for free.
	 * Opportuniy for visualization.
	 * @param player The player.
	 * @param hotel The hotels to be upgraded.
	 */
	void freeBuildingApproved(Player player, Hotel hotel);

	/**
	 * Called when a building's upgrade is approved with double the cost.
	 * Opportuniy for visualization.
	 * @param player The player.
	 * @param hotel The hotels to be upgraded.
	 */
	void overCostBuildingApproved(Player player, Hotel hotel);

	/**
	 * Called when the game is over.
	 * Opportuniy for visualization.
	 * @param player The winning player.
	 */
	void gameOver(Player player);
}
