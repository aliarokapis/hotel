package hotel.ui;

import javafx.application.*;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.text.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.shape.*;
import javafx.scene.canvas.*;
import javafx.scene.paint.*;
import javafx.beans.binding.*;
import javafx.beans.property.*;
import java.util.Random;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.concurrent.CompletableFuture;
import javafx.concurrent.Task;

import hotel.GameState;
import hotel.util.function.*;
import hotel.ui.Dice;
import hotel.Player;
import hotel.Hotel;
import hotel.Board;
import hotel.Game;

import javafx.fxml.FXML;


/** 
 * A Class implementing the UI interface for a JavaFx application.
 * Because the UI interface calls will be called from the gameloop's thread,
 * the graph-modification calls must be wrapped into @see Platform#runLater.
 */

public class UIController implements UI
{
	/** 
	 * Constructor.
	 * @param state The GameState required to display the game.
	 */
	public UIController(GameState state)
	{
		this.dice = new Dice();
		this.playerColors = new HashMap<String, String>();
		this.stackMap = new HashMap<Board.Coordinates, StackPane>();
		this.rollDiceButton = new Button();
		this.random = new Random();
		this.state = state;
	}

	public void initialize()
	{
		rollDiceButton.setText("Roll Dice!");
		rollDiceButton.setDisable(true);

		rightVBox.getChildren().add(1, dice.getImageView());
		rightVBox.getChildren().add(2, rollDiceButton);


		for (Player player : state.getPlayers())
		{
			Label info = new Label(String.format("%s: %d MLs", player.getName(), player.getBalance()));
			info.setUserData(player);
			player.getBalanceProperty().addListener(evt -> Platform.runLater(() -> info.setText(String.format("%s: %d MLs", player.getName(), player.getBalance()))));
			playerInfo.getChildren().add(info);
		}

		grid.setPrefWidth(grid.getWidth());

		List<String> colors = Arrays.asList(
			"lightblue",
			"burlywood",
			"lightgreen");

		int i = 0;

		for (Player player : state.getPlayers())
		{
			if (i >= colors.size())
			{
				colors.add(String.format("rgb(%d,%d,%d)", random.nextInt(256), random.nextInt(256), random.nextInt(256)));
			}

			playerColors.put(player.getName(), colors.get(i));

			++i;
		}

		for (int row = 0; row != state.getBoard().rows(); ++row)
		{
			for (int col = 0; col != state.getBoard().columns(); ++col)
			{
				StackPane stack = new StackPane();
				stackMap.put(new Board.Coordinates(col, row), stack);
				grid.getChildren().add(stack);
			}
		}

		grid.heightProperty().addListener(e -> Platform.runLater(() -> resetMap()));
		grid.widthProperty().addListener(e -> Platform.runLater(() -> resetMap()));

		availableHotels.setText(String.format("Available Hotels: %d", state.getHotels().size()));

		for (Hotel hotel : state.getHotels())
		{
			hotel.getLevelProperty().addListener(e -> {
				Platform.runLater(() -> {
				long c = state.getHotels().stream().filter(h -> !h.isBuilt()).count();
				availableHotels.setText(String.format("Available Hotels: %d", c));
				});
			});
		}

		resetMap();
	}

	double columnToX(int column)
	{
		return column * tileWidth();
	}

	double rowToY(int row)
	{
		return row * tileHeight();
	}

	double tileWidth()
	{
		return grid.getWidth() / state.getBoard().columns();
	}

	double tileHeight()
	{
		return grid.getHeight() / state.getBoard().rows();
	}

	void resetMap()
	{
		for (int row = 0; row != state.getBoard().rows(); ++row)
		{
			for (int col = 0; col != state.getBoard().columns(); ++col)
			{
				resetTile(row, col);
			}
		}

		for (Player player : state.getPlayers())
		{
			Board.Coordinates coords = state.getPath().get(player.getLocation());
			drawPlayer(coords.y, coords.x, player);
		}

		for (int location = 0; location != state.getPath().size(); ++location)
		{
			Optional<Hotel> entrance = state.getHotelEntrance(location);
			if (entrance.isPresent())
			{
				drawEntrance(location, entrance.get());
			}
		}

		for (Hotel hotel : state.getHotels())
		{
			List<Board.Coordinates> coords = state.getBoard()
							      .getCoordinatesOfPiece(
								Integer.toString(hotel.getId()));

			for (Board.Coordinates c : coords)
			{
				drawHotel(c, hotel);
			}
		}
	}


	void drawPlayer(int row, int col, Player player)
	{
		double x = columnToX(col);
		double y = rowToY(row);

		String color = playerColors.get(player.getName());

		List<Player> sameCell = 
			state.getPlayers()
			     .stream()
			     .filter(p -> p.getLocation() == player.getLocation())
			     .collect(Collectors.toList());
		
		int id = sameCell.indexOf(player);

		Double radius = tileWidth()/8;

		double theta = 2 * Math.PI / sameCell.size() * id;

		double radiusOffset = tileWidth() / 4;

		double xOffset = radiusOffset * Math.cos(theta);
		double yOffset = radiusOffset * Math.sin(theta);

		Circle circle = new Circle(radius, Color.web(color));
		circle.setStroke(Color.BLACK);
		circle.setStrokeWidth(1);

		if (sameCell.size() != 1)
		{
		    circle.setTranslateX(xOffset);
		    circle.setTranslateY(yOffset);
		}

		Board.Coordinates coords = state.getPath().get(player.getLocation());
		StackPane stack = stackMap.get(coords);
		stack.getChildren().add(circle);
		stack.toFront();
	}

	void drawEntrance(Integer location , Hotel hotel)
	{
		Board.Coordinates coords = state.getPath().get(location);

		StackPane stack = new StackPane();

		Rectangle rect = new Rectangle(tileWidth()/5, tileHeight()/5, Color.BLACK);

		Text text = new Text(Integer.toString(hotel.getId()));
		text.setStroke(Color.WHITE);
		
		stack.getChildren().addAll(rect, text);

		StackPane tile = stackMap.get(coords);

		tile.getChildren().add(stack);
		stack.setTranslateX(-tileWidth()/2 + tileWidth()/5);
		stack.setTranslateY(tileHeight()/2 - tileHeight()/5);
	}

	void drawHotel(Board.Coordinates coords, Hotel hotel)
	{
		StackPane stack = stackMap.get(coords);

		Text name = new Text(hotel.getName());

		Text id = new Text("id: " + Integer.toString(hotel.getId()));
		stack.getChildren().add(id);
		id.setTranslateY(25);
		id.setTranslateX(-20);

		stack.getChildren().add(name);
		
		if (hotel.getOwner().isPresent())
		{
			Text owner = new Text(hotel.getOwner().get().getName());
			stack.getChildren().add(owner);
			owner.setTranslateY(-25);

		}


		Text level = new Text("lvl: " + hotel.getLevel());
		stack.getChildren().add(level);
		level.setTranslateY(25);
		level.setTranslateX(20);
	}

	void resetTile(int row, int col)
	{
		double x = columnToX(col);
		double y = rowToY(row);

		Label label = new Label("test");

		Rectangle rect = new Rectangle(tileWidth(), tileHeight(), Color.BLACK);
		rect.setStroke(Color.WHITE);
		rect.setStrokeWidth(2);
		rect.setFill(Color.LIGHTGRAY);

		String text = state.getBoard().getPieceAt(col, row);

		if (text.equals("H"))
			rect.setFill(Color.GRAY);
		if (text.equals("E"))
			rect.setFill(Color.BROWN);
		if (text.equals("B"))
			rect.setFill(Color.CRIMSON);
		if (text.equals("S"))
			rect.setFill(Color.ORANGE);
		if (text.equals("C"))
			rect.setFill(Color.PURPLE);

		StackPane stack = stackMap.get(new Board.Coordinates(col, row));
		stack.getChildren().clear();
		stack.getChildren().add(rect);

		if (List.of("H", "E", "B", "S", "C").contains(text))
		{
		    Text textNode = new Text(text);
		    textNode.setFill(Color.BLACK);
		    stack.getChildren().add(textNode);
		}

		stack.relocate(x, y);
		stack.toFront();
	}

	public void newTurn(Player player)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {

		logger.appendText(String.format("It's %s's turn.\n", player.getName()));

		for (Node node : playerInfo.getChildren())
		{
			Label label = (Label)node;
			
			Player data = (Player)label.getUserData();

			if (player == data)
			{
				label.setStyle(String.format("-fx-font-weight: bold; -fx-background-color: %s", playerColors.get(player.getName())));
			}
			else
			{
				label.setStyle(String.format("-fx-background-color: %s", playerColors.get(data.getName())));
			}
		}

		});
	}

	public Integer rollDice(Random random)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

                CompletableFuture<Integer> future = new CompletableFuture<>();
		Platform.runLater(() -> {
		    rollDiceButton.setDisable(false);
		    rollDiceButton.setOnAction(e -> {
			Integer d = random.nextInt(6)+1;
			dice.rollDice(d);
			rollDiceButton.setDisable(true);
			future.complete(d);
		    });
		});
		try {
		    return future.get();
		} catch (Exception e) {
		    return 1;
		}

	}

	public void movePlayer(Player player, Integer nextLocation)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {
			resetMap();
		});
	}

	public void playerIsPaidByBank(Player player, Integer payment)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> logger.appendText(String.format("Player %s is paid %d MLs by the Bank\n", player.getName(), payment)));
	}

	void listHotels(List<Hotel> hotels)
	{
		for (Hotel hotel : hotels)
		{
			String res   = String.format("Hotel %s:     \n", hotel.getName());

			if (!hotel.isBuilt())
			{
				res += String.format("    UNBUILT\n");
				if (!hotel.getOwner().isPresent())
					res += String.format("    PURCHASE PRICE: %d\n",
							hotel.getFreePurchasePrice());
				else
					res += String.format("    PURCHASE PRICE: %d\n",
							hotel.getForcedPurchasePrice());
					
				res += String.format("    BUILDING PRICE: %d\n",
						hotel.getBuildingCost());
			}
			else if (hotel.canUpgrade())
			{
				res += String.format("    UPGRADE PRICE: %d\n",
						hotel.getBuildingCost());
			}

			if (hotel.isBuilt())
			{
				res += String.format("    ENTRANCE PRICE: %d\n",
						hotel.getChargingPrice());

				res += String.format("    PUT ENTRANCE COST: %d\n",
						hotel.getBuildEntranceCost());
			}

			
			
			res += String.format("    ID: %d\n", hotel.getId());

			res += String.format("    LEVEL: %d\n", hotel.getLevel());

			res += String.format("    OWNER: %s\n", hotel.getOwner()
								     .map(o -> o.getName())
								     .orElse("None"));

			logger.appendText(res);
		}
	}

	void clearMarkings()
	{
		for (StackPane panes : stackMap.values())
		{
			panes.setOnMouseClicked(null);
		}

		resetMap();
	}

	void markHotel(Hotel hotel, Runnable callback)
	{
		Platform.runLater(() -> {
		Integer id = hotel.getId();
		List<Board.Coordinates> coords = state.getBoard().getCoordinatesOfPiece(Integer.toString(id));

		Color color = Color.color(Math.random(), Math.random(), Math.random());

		for (Board.Coordinates c : coords)
		{
			StackPane stack = stackMap.get(c);
			Rectangle outline = new Rectangle(tileWidth(), tileHeight(), Color.TRANSPARENT);
			outline.setStroke(color);
			outline.setStrokeWidth(5);
			stack.getChildren().add(outline);
			stack.setOnMouseClicked(e -> callback.run());
			stack.toFront();
		}
		});
	}

	public void showAvailableHotelsForEntrance(Player player, List<Hotel> hotels)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {
		logger.appendText(String.format("%s, you can put an entrance for the following hotels:\n", player.getName()));
		listHotels(hotels);
		});
	}

	public void showAvailableHotelsForUpgrade(Player player, List<Hotel> hotels)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {
		logger.appendText(String.format("%s, you can upgrade the following hotels:\n", player.getName()));
		listHotels(hotels);
		});
	}

	public void showAvailableHotelsForPurchase(Player player, List<Hotel> hotels)
        {
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {
		logger.appendText(String.format("%s, you can purchase the following hotels:\n", player.getName()));
		listHotels(hotels);
		});
        }

	public void setEntrance(Player player, Integer location, Hotel hotel)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {
		logger.appendText(String.format("%s puts entrance of %s at %d\n", 
			player.getName(),
			hotel.getName(),
			location));
		
		resetMap();
		});

	}

	int chooseOption(String first, String second, Optional<String> mthird)
	{
		CompletableFuture<Integer> future = new CompletableFuture<>();
		Platform.runLater(() -> {
		    HBox hbox = new HBox();
		    hbox.setAlignment(Pos.CENTER);
		    hbox.setSpacing(10.0);
		    Button bfirst = new Button(first);
		    bfirst.setOnAction(e -> {
			future.complete(1);
			rightVBox.getChildren().remove(hbox);
		    });
			
		    Button bsec = new Button(second);
		    bsec.setOnAction(e -> {
			future.complete(2);
			rightVBox.getChildren().remove(hbox);
		    });

		    hbox.getChildren().addAll(bfirst, bsec);

		    if (mthird.isPresent())
		    {
			Button bthird = new Button(mthird.get());
			bthird.setOnAction(e -> {
			    future.complete(0);
			    rightVBox.getChildren().remove(hbox);
			});

		    	hbox.getChildren().add(bthird);
		    }
			
		    rightVBox.getChildren().add(4, hbox);
		});

		try 
		{
			return future.get();
		} 
		catch (Exception e)
		{
			return 0;
		}
	}

	public Boolean chooseToPutEntrance(Player player)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		return chooseOption("put entrance", "don't put entrance", Optional.empty()) == 1;
	}

	public Boolean chooseToPurchase(Player player)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		return chooseOption("purchase", "don't purchase", Optional.empty()) == 1;
	}

	public Integer chooseUpgradeEntranceOrBoth(Player player, Boolean entrance, Boolean upgrade)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		if (upgrade && !entrance)
		{
			if (chooseOption("upgrade", "don't upgrade", Optional.empty()) == 1)
				return 1;
		}
		else if (entrance && !upgrade)
		{
			if (chooseOption("put entrance", "don't put entrance", Optional.empty()) == 1)
				return 2;
		}
		else if (upgrade && entrance)
		{
			return chooseOption("upgrade", "put entrance", Optional.of("nothing"));
		}

		return 0;
	}

	public Optional<Hotel> chooseHotel(Player player, List<Hotel> hotels)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		CompletableFuture<Optional<Hotel>> future = new CompletableFuture<>();
		Platform.runLater(() -> {
			for (Hotel hotel : hotels)
				markHotel(hotel, () -> {
					future.complete(Optional.of(hotel));
					clearMarkings();
				});

		});
		try {
			return future.get();
		} catch (Exception e)
		{	
			return Optional.empty();
		}
	}

	public void playerPurchased(Player player, Hotel hotel)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {
                logger.appendText(String.format("%s purchased %s\n", player.getName(), hotel.getName()));

		resetMap();

		});
	}

        public void playerRepurchasedFrom(Player player, Player owner, Hotel hotel)
        {
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {
                logger.appendText(String.format("%s forcibly repurchased %s from %s\n", 
                        player.getName(), hotel.getName(), owner.getName()));

		resetMap();
		});
        }

	public void playerPayment(Player player, Player owner, Integer payment)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {
		logger.appendText(String.format("%s pays %s %d MLs\n", player.getName(), owner.getName(), payment));
		});
	}

	public void bankruptcy(Player player)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {
		logger.appendText(String.format("%s is bankrupt...\n", player.getName()));

		resetMap();

		});
	}

	public void normalBuildingApproved(Player player, Hotel hotel)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {
		logger.appendText(String.format("Normal build\n"));
		
		resetMap();

		});
	}

	public void buildingNotApproved(Player player, Hotel hotel)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {
		logger.appendText(String.format("Not approved\n"));
		
		resetMap();
		});
	}

	public void freeBuildingApproved(Player player, Hotel hotel)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {
		logger.appendText(String.format("Free build\n"));
		
		resetMap();
		});
	}

	public void overCostBuildingApproved(Player player, Hotel hotel)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {
		logger.appendText(String.format("Over cost build\n"));
		
		resetMap();
		});
	}

	public void gameOver(Player player)
	{
		try {
		Thread.sleep(500);
		} catch (Exception e) {}

		Platform.runLater(() -> {
		logger.appendText(String.format("The game is over! %s wins!!\n", player.getName()));
		});
	}

	@FXML
	public void cards() {

		VBox vbox = new VBox();

		for (Hotel hotel : state.getHotels())
		{
			HotelView view = new HotelView(hotel);
			vbox.getChildren().add(view);
		}
		
		ScrollPane scroll = new ScrollPane();

		scroll.setFitToWidth(true);
		scroll.setPrefWidth(450);
		scroll.setPrefHeight(500);

		scroll.setContent(vbox);

		Stage stage = new Stage();
		Scene scene = new Scene(scroll);
		stage.setScene(scene);
		stage.show();
	}

	@FXML
	public void exit() 
	{
		Platform.exit();
	}

	@FXML
	private BorderPane borderPane;

	@FXML
	private VBox rightVBox;

	@FXML
	private HBox playerInfo;

	@FXML
	private TextArea logger;

	@FXML
	private Pane grid;

	@FXML
	private Label availableHotels;

	private GameState state;
	private Dice dice;
	private Button rollDiceButton;

	private Map<String, String> playerColors;

	private Map<Board.Coordinates, StackPane> stackMap;

	private Map<Board.Coordinates, StackPane> menuMap;;
	private Random random;

}
