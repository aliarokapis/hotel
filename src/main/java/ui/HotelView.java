package hotel.ui;

import hotel.Hotel;
import hotel.Player;

import javafx.geometry.*;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.collections.*;
import javafx.beans.property.*;

import java.util.Optional;

/**
 * A Class providing a view of a Hotel's attributes.
 */
public class HotelView extends StackPane
{
	static private Label createLabel(String text)
	{
		Label label = new Label(text);
		label.setStyle("-fx-font: NORMAL 14 Tahoma");
		label.setAlignment(Pos.CENTER);
		GridPane.setHalignment(label, HPos.CENTER);
		return label;
	}

	/**
	 * Constructor.
	 * @param hotel The hotel to represent.
	 */
	public HotelView(Hotel hotel)
	{
		this.hotel = hotel;
		this.hotel.getOwnerProperty().addListener(e -> draw());
		this.hotel.getLevelProperty().addListener(e -> draw());
		draw();
	}

	private void draw()
	{
		VBox vbox = new VBox();
		vbox.setPadding(new Insets(10.0, 10.0, 10.0, 10.0));
		vbox.setAlignment(Pos.CENTER);

		StackPane header = new StackPane();
		header.setAlignment(Pos.CENTER);
		header.setPadding(new Insets(10.0, 10.0, 10.0, 10.0));
		header.setStyle("-fx-border-color: black; -fx-border-width: 1 1 0 1;");

		Label name = new Label("Hotel " + hotel.getName());
		name.setStyle("-fx-font: NORMAL 20 Tahoma; -fx-font-weight: bold;");
		header.getChildren().add(name);

		vbox.getChildren().add(header);

		GridPane grid = new GridPane();
		grid.setStyle("-fx-border-color: black; -fx-border-width: 1 1 0 1;");
		grid.setPadding(new Insets(10.0, 10.0, 10.0, 10.0));
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10.0);
		grid.setVgap(10.0);


		int lineC = 0;
		grid.addRow(lineC++, createLabel("Id:"), createLabel(Integer.toString(hotel.getId())));

		Optional<Player> mowner = hotel.getOwner();

		if (mowner.isPresent())
		{
			grid.addRow(lineC++, createLabel("Owner :"), createLabel(mowner.get().getName()));
		}
		else if (hotel.getBoughtByBank())
		{
			grid.addRow(lineC++, createLabel("Owner :"), createLabel("Bank"));
		}
		else
		{
			grid.addRow(lineC++, createLabel("Owner :"), createLabel("None"));
		}

		if (!hotel.isBuilt())
		{
			grid.addRow(lineC++, createLabel("Building status:"), createLabel("unbuilt"));
		}
		else if (hotel.canUpgrade())
		{
			grid.addRow(lineC++, createLabel("Building status:"), createLabel("can upgrade"));
		}
		else 
		{
			grid.addRow(lineC++, createLabel("Building status:"), createLabel("completed"));
		}

		if (!hotel.isBuilt())
		{
			grid.addRow(lineC++, createLabel("Free purchase price:"), createLabel(Integer.toString(hotel.getFreePurchasePrice())));
			grid.addRow(lineC++, createLabel("Forced purchase price:"), createLabel(Integer.toString(hotel.getForcedPurchasePrice())));
		}

		grid.addRow(lineC++, createLabel("Put entrance cost:"), createLabel(Integer.toString(hotel.getBuildEntranceCost())));
		grid.addRow(lineC++, createLabel("Level:"), createLabel(Integer.toString(hotel.getLevel())));
		grid.addRow(lineC++, createLabel("Max Level:"), createLabel(Integer.toString(hotel.getPrices().size())));

		if (hotel.canUpgrade())
		{
			grid.addRow(lineC++, createLabel("Upgrade cost:"), createLabel(Integer.toString(hotel.getBuildingCost())));
		}
	

		vbox.getChildren().add(grid);

		GridPane info = new GridPane();
		info.setStyle("-fx-border-color: black;");
		info.setPadding(new Insets(10.0, 10.0, 10.0, 10.0));
		info.setAlignment(Pos.CENTER);
		info.setHgap(10.0);
		info.setVgap(10.0);

		Label level = new Label("Level");
		level.setStyle("-fx-font: NORMAL 16 Tahoma; -fx-font-weight: bold;");
		level.setAlignment(Pos.CENTER);

		Label upgradeCost = new Label("Upgrade Cost");
		upgradeCost.setStyle("-fx-font: NORMAL 16 Tahoma; -fx-font-weight: bold;");
		upgradeCost.setAlignment(Pos.CENTER);

		Label entrancePrice = new Label("Entrance Price");
		entrancePrice.setStyle("-fx-font: NORMAL 16 Tahoma; -fx-font-weight: bold;");
		entrancePrice.setAlignment(Pos.CENTER);

		info.addRow(0, level, upgradeCost, entrancePrice);

		lineC = 1;

		for (int i = 0; i != hotel.getPrices().size(); ++i)
		{
			info.addRow(lineC++, createLabel(Integer.toString(i+1)), 
					     createLabel(Integer.toString(hotel.getPrices().get(i).getBuildingCost())),
					     createLabel(Integer.toString(hotel.getPrices().get(i).getChargingPrice())));
		}
		
		vbox.getChildren().add(info);

		getChildren().add(vbox);
	}

	private Hotel hotel;
}

