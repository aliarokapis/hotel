package hotel.ui;

import hotel.ui.UI;
import hotel.GameState;
import hotel.Player;
import hotel.Hotel;
import hotel.util.function.*;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * A Class that implements the UI interface for Stdin/Stdout.
 */

public class TerminalUI implements UI
{
	public TerminalUI(GameState state)
	{
		this.state = state;
		this.reader = new BufferedReader(new InputStreamReader(System.in));
	}

	public void newTurn(Player player)
	{
		System.out.printf("It's %s's turn.\n", player.getName());
	}

	public Integer rollDice(Random random)
	{
	        Integer d = random.nextInt(6) + 1;
		System.out.printf("Rolling dice animation... shows (%d)\n", d);
                return d;
	}

	public void movePlayer(Player player, Integer nextLocation)
	{
		System.out.printf("Player %s moves to location %d\n", player.getName(), nextLocation); 
	}

	public void playerIsPaidByBank(Player player, Integer payment)
	{
		System.out.printf("Bank pays %s %d ML\n", player.getName(), payment);
	}


	void listHotels(List<Hotel> hotels)
	{
		for (Hotel hotel : hotels)
		{
			System.out.printf("Hotel name: %s, id: %d, owner: %s, level: %d\n", 
				hotel.getName(), 
				hotel.getId(), 
				hotel.getOwner()	
				     .map(o -> o.getName())
				     .orElse("none"),
				hotel.getLevel());
		}
	}

	public void showAvailableHotelsForEntrance(Player player, List<Hotel> hotels)
	{
		System.out.printf("%s, you can put an entrance for the following hotels:\n", player.getName());
		listHotels(hotels);
	}

	public void showAvailableHotelsForUpgrade(Player player, List<Hotel> hotels)
	{
		System.out.printf("%s, you can upgrade the following hotels:\n", player.getName());
		listHotels(hotels);
	}

	public void showAvailableHotelsForPurchase(Player player, List<Hotel> hotels)
        {
		System.out.printf("%s, you can purchase the following hotels:\n", player.getName());
		listHotels(hotels);
        }


	public Boolean chooseToPutEntrance(Player player)
	{
		System.out.printf("%s, do you want to put an entrance?\n", player.getName());
		String res = null;
		try {
		res = reader.readLine();
		} catch (Exception e) { return false; }
		return res.equals("yes") || res.equals("Yes");
	}

        public Boolean chooseToPurchase(Player player)
        {
		System.out.printf("%s, do you want to purchase?\n", player.getName());
		String res = null;
		try {
		res = reader.readLine();
		} catch (Exception e) { return false; }
		return res.equals("yes") || res.equals("Yes");
        }

        public void playerPurchased(Player player, Hotel hotel)
        {
                System.out.printf("%s purchased %s\n", player.getName(), hotel.getName());
        }

        public void playerRepurchasedFrom(Player player, Player owner, Hotel hotel)
        {
                System.out.printf("%s forcibly repurchased %s from %s\n", 
                        player.getName(), hotel.getName(), owner.getName());
        }

	public Optional<Hotel> chooseHotel(Player player, List<Hotel> hotels)
	{
		System.out.println("Input hotel id:");
		String res = null;
		try {
		res = reader.readLine();
		} catch (Exception e) { return Optional.empty(); }
		Integer id = null;
		try {
		id = Integer.parseInt(res);
		} catch (NumberFormatException e) { return Optional.empty();}
		return this.state.hotelWithId(id)
				 .filter(h -> hotels.contains(h));
	}

	public void setEntrance(Player player, Integer location, Hotel hotel)
	{
		System.out.printf("Added hotel %s entance at %d\n", hotel.getName(), location);	
	}

	public void playerPayment(Player player, Player owner, Integer payment)
	{
		System.out.printf("%s pays %s %d MLs\n", player.getName(), owner.getName(), payment);
	}
	
	public void bankruptcy(Player player)
	{
		System.out.printf("%s is bankrupt...\n", player.getName());
	}

	public Integer chooseUpgradeEntranceOrBoth(Player player, Boolean entrance, Boolean upgrade)
	{
		if (entrance && !upgrade)
			System.out.println("entrance ?");
		else if (!entrance && upgrade)
			System.out.println("upgrade ?");
		else if (entrance && upgrade)
			System.out.println("upgrade, entrance or neither ?");
		else return 0;

		String res = null;
		try {
		res = reader.readLine();
		} catch (Exception e) { return 0; }

		if (!entrance && upgrade)
		{
			if (res.equals("yes"))
				return 1;
		}
		else if (entrance && !upgrade)
		{
			if (res.equals("yes"))
				return 2;
		}
		else if (entrance && upgrade)
		{
			if (res.equals("upgrade"))
				return 1;
			else if (res.equals("entrance"))
				return 2;
		}

		return 0;
	}

	public void normalBuildingApproved(Player player, Hotel hotel)
	{
		System.out.println("Normal build");
	}

	public void buildingNotApproved(Player player, Hotel hotel)
	{
		System.out.println("Not approved");
	}

	public void freeBuildingApproved(Player player, Hotel hotel)
	{
		System.out.println("Free build");
	}

	public void overCostBuildingApproved(Player player, Hotel hotel)
	{
		System.out.println("Over cost build");
	}

	public void gameOver(Player player)
	{
		System.out.printf("The game is over! %s wins!!\n", player.getName());
	}

	private GameState state;
	private BufferedReader reader;

}
